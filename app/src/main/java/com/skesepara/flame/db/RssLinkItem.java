package com.skesepara.flame.db;

public class RssLinkItem {
    private final String mLink;

    private final String mCategoryId;

    private final String mSubCategoryId;

    private final String mPattern;

    public RssLinkItem(String link, String categoryId, String subCategoryId, String pattern) {
        mLink = link;
        mCategoryId = categoryId;
        mSubCategoryId = subCategoryId;
        mPattern = pattern;
    }

    public String getLink() {
        return mLink;
    }

    public String getCategoryId() {
        return mCategoryId;
    }

    public String getSubCategoryId() {
        return mSubCategoryId;
    }

    public String getPattern() {
        return mPattern;
    }
}
