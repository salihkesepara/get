package com.skesepara.flame.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class TableSettings {
    private static final String TAG = TableSettings.class.getSimpleName();
    public static final String IMAGE_DOWNLOADABLE = "1";
    public static final String IMAGE_NOT_DOWNLOADABLE = "0";
    public static final String FIRST_OPEN = "1";
    public static final String NOT_FIRST_OPEN = "0";

    /* Table name */
    private final String TABLE_NAME = "settings";

    /* Table rows */
    private final String IS_IMAGE_DOWNLOADABLE = "isImageDownloadable";
    private final String IS_FIRST_OPEN = "isFirstOpen";

    /* Our DbAdapter object */
    private DbAdapter mDbAdapter;

    /* MainActivity Context data */
    private Context mContext;

    public TableSettings(DbAdapter dbAdapter) {
        mDbAdapter = dbAdapter;
    }

    /* Create table query */
    public final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "("
            + IS_IMAGE_DOWNLOADABLE + " TEXT, "
            + IS_FIRST_OPEN + " TEXT) ";

    /* Drop table query */
    public final String DROP_TABLE = "DROP TABLE " + TABLE_NAME;

    public void updateDownloadImageRule(String isImagesDownloaded) {
        SQLiteDatabase db = mDbAdapter.getWritableDatabase();
        try {
            ContentValues cv = new ContentValues();
            cv.put(IS_IMAGE_DOWNLOADABLE, isImagesDownloaded);
            db.update(TABLE_NAME, cv, null, null);
        } catch (Exception e) {
            Log.e(TAG, "setDownloadImageRule: " + e.getMessage());
        }

        db.close();
    }

    public void updateFirstOpen(String isFirstOpen) {
        SQLiteDatabase db = mDbAdapter.getWritableDatabase();
        try {
            ContentValues cv = new ContentValues();
            cv.put(IS_FIRST_OPEN, isFirstOpen);
            db.update(TABLE_NAME, cv, null, null);
        } catch (Exception e) {
            Log.e(TAG, "updateFirstOpen: " + e.getMessage());
        }

        db.close();
    }

    public void setDownloadImageRule(String isImagesDownloaded, String isFirstOpen) {
        SQLiteDatabase db = mDbAdapter.getWritableDatabase();
        try {
            String sql = "insert into "+ TABLE_NAME +"("+ IS_IMAGE_DOWNLOADABLE +", "+ IS_FIRST_OPEN +") " +
                    "select ?, ? " +
                    "where not exists(select 1 from "+ TABLE_NAME +")";
            String[] bindArgs = {isImagesDownloaded, isFirstOpen};

            db.execSQL(sql, bindArgs);
        } catch (Exception e) {
            Log.e(TAG, "setDownloadImageRule: " + e.getMessage());
        }

        db.close();
    }

    public String getIsImageDownloadable() {
        SQLiteDatabase db = mDbAdapter.getReadableDatabase();
        String isImageDwonloadable = "1";

        try {
            String[] columns = {IS_IMAGE_DOWNLOADABLE};
            Cursor cursor = db.query(TABLE_NAME, columns, null, null,
                    null, null, null, null);
            while (cursor.moveToNext()) {
                isImageDwonloadable = cursor.getString(0);
            }
        } catch (Exception e) {
            Log.e(TAG, "getIsImageDownloadable: " + e.getMessage());
        }

        db.close();
        return isImageDwonloadable;
    }

    public String getFirstOpen() {
        SQLiteDatabase db = mDbAdapter.getReadableDatabase();
        String isFirstOpen = "0";

        try {
            String[] columns = {IS_FIRST_OPEN};
            Cursor cursor = db.query(TABLE_NAME, columns, null, null,
                    null, null, null, null);
            while (cursor.moveToNext()) {
                isFirstOpen = cursor.getString(0);
            }
        } catch (Exception e) {
            Log.e(TAG, "getFirstOpen: " + e.getMessage());
        }

        db.close();
        return isFirstOpen;
    }

    public void fillTable() {
        setDownloadImageRule(IMAGE_DOWNLOADABLE, FIRST_OPEN);
    }
}
