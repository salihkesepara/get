package com.skesepara.flame.db;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

public class TableLink {
    private static final String TAG = TableLink.class.getSimpleName();
    private static final String REFRESH_TIME = "-5 minutes";

    /* Table name */
    private final String TABLE_NAME = "link";

    /* Table rows */
    private final String ID = "id";
    private final String LINK = "link";
    private final String CATEGORY_ID = "categoryId";
    private final String SUB_CATEGORY_ID = "subCategoryId";
    private final String CREATE_DATE = "createDate";

    /* Our DbAdapter object */
    private DbAdapter mDbAdapter;

    public TableLink(DbAdapter dbAdapter) {
        mDbAdapter = dbAdapter;
    }

    /* Create table query */
    public final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "("
            + ID + " INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE, "
            + LINK+ " TEXT UNIQUE, "
            + CATEGORY_ID+ " TEXT, "
            + SUB_CATEGORY_ID+ " TEXT, "
            + CREATE_DATE + " TEXT) ";

    /* Drop table query */
    public final String DROP_TABLE = "DROP TABLE " + TABLE_NAME;

    public void addRow(String link, String categoryId, String subCategoryId) {
        SQLiteDatabase db = mDbAdapter.getWritableDatabase();
        try {
            String sql = "insert into "+ TABLE_NAME +"("+ LINK +", "+ CATEGORY_ID +", "+ SUB_CATEGORY_ID +", "+ CREATE_DATE +") " +
                    "select ?, ?, ?, '' " +
                    "where not exists(select 1 from "+ TABLE_NAME +" where "+ LINK +" = ?)";
            String[] bindArgs = new String[]{
                    link, String.valueOf(categoryId), String.valueOf(subCategoryId), link};

            db.execSQL(sql, bindArgs);
        } catch (Exception e) {
            Log.e(TAG, "addRow: Exception: " + e.getMessage());
        }

        db.close();
    }

    public ArrayList<RssLinkItem> getData() {
        SQLiteDatabase db = mDbAdapter.getReadableDatabase();
        ArrayList<RssLinkItem> rssLinkItem = new ArrayList<>();

        try {
            String sql = "select link."+ LINK +", link."+ CATEGORY_ID +", link."+ SUB_CATEGORY_ID +", category.pattern " +
                    "from link " +
                    "inner join category on link."+ CATEGORY_ID +" = category.id " +
                    "inner join subCategory on link."+ SUB_CATEGORY_ID +" = subCategory.id " +
                    "where category.active = '1' and subCategory.active = '1' and link.createDate < datetime('now', 'localtime', '"+ REFRESH_TIME +"')";
            String[] selectionArgs = {};

            Cursor cursor = db.rawQuery(sql, selectionArgs);
            Log.v(TAG, TABLE_NAME + ": getData: " + Integer.toString(cursor.getCount()));

            while (cursor.moveToNext()) {
                rssLinkItem.add(new RssLinkItem(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3)));
            }
        } catch (Exception e) {
            Log.e(TAG, "getData: " + e.getMessage());
        }

        db.close();
        return rssLinkItem;
    }

    public void updateCreateDate(String categoryId, String subCategoryId) {
        SQLiteDatabase db = mDbAdapter.getWritableDatabase();

        try {
            String sql = "update link set createDate = datetime('now', 'localtime') where categoryId=? and subCategoryId=?";
            String[] bindArgs = new String[]{ categoryId, subCategoryId };

            db.execSQL(sql, bindArgs);
        } catch (Exception e) {
            Log.e(TAG, "updateCreateDate: Exception: " + e.getMessage());
        }

        db.close();
    }

    public void fillTable() {
        // CNN Türk
        addRow("https://www.cnnturk.com/feed/rss/all/news", TableCategory.CNN, TableSubCategory.HOME);
        addRow("https://www.cnnturk.com/feed/rss/dunya/news", TableCategory.CNN, TableSubCategory.WORLD);
        addRow("https://www.cnnturk.com/feed/rss/kultur-sanat/news", TableCategory.CNN, TableSubCategory.ART_AND_CUSTURE);
        addRow("https://www.cnnturk.com/feed/rss/bilim-teknoloji/news", TableCategory.CNN, TableSubCategory.TECHNOLOGY);
        addRow("https://www.cnnturk.com/feed/rss/otomobil/news", TableCategory.CNN, TableSubCategory.CAR);
        addRow("https://www.cnnturk.com/feed/rss/ekonomi/news", TableCategory.CNN, TableSubCategory.ECONOMY);
        addRow("https://www.cnnturk.com/feed/rss/saglik/news", TableCategory.CNN, TableSubCategory.HEALT);
        addRow("https://www.cnnturk.com/feed/rss/spor/news", TableCategory.CNN, TableSubCategory.SPORT);
        addRow("https://www.cnnturk.com/feed/rss/seyahat/news", TableCategory.CNN, TableSubCategory.TRAVEL);
        addRow("https://www.cnnturk.com/feed/rss/yazarlar", TableCategory.CNN, TableSubCategory.WRITER);
        addRow("https://www.cnnturk.com/feed/rss/magazin/news", TableCategory.CNN, TableSubCategory.MAGAZINE);

        // Yeni Şafak
        addRow("https://www.yenisafak.com/rss", TableCategory.YENISAFAK, TableSubCategory.HOME);
        addRow("https://www.yenisafak.com/rss?xml=spor", TableCategory.YENISAFAK, TableSubCategory.SPORT);
        addRow("https://www.yenisafak.com/rss?xml=ekonomi", TableCategory.YENISAFAK, TableSubCategory.ECONOMY);
        addRow("https://www.yenisafak.com/rss?xml=teknoloji", TableCategory.YENISAFAK, TableSubCategory.TECHNOLOGY);
        addRow("https://www.yenisafak.com/rss?xml=dunya", TableCategory.YENISAFAK, TableSubCategory.WORLD);
        addRow("https://www.yenisafak.com/rss?xml=yazarlar", TableCategory.YENISAFAK, TableSubCategory.WRITER);
        addRow("https://www.yenisafak.com/rss?xml=hayat", TableCategory.YENISAFAK, TableSubCategory.TRAVEL);

        // Hürriyet
        addRow("http://www.hurriyet.com.tr/rss/anasayfa", TableCategory.HURRIYET, TableSubCategory.HOME);
        addRow("http://www.hurriyet.com.tr/rss/ekonomi", TableCategory.HURRIYET, TableSubCategory.ECONOMY);
        addRow("http://www.hurriyet.com.tr/rss/spor", TableCategory.HURRIYET, TableSubCategory.SPORT);
        addRow("http://www.hurriyet.com.tr/rss/teknoloji", TableCategory.HURRIYET, TableSubCategory.TECHNOLOGY);
        addRow("http://www.hurriyet.com.tr/rss/saglik", TableCategory.HURRIYET, TableSubCategory.HEALT);
        addRow("http://www.hurriyet.com.tr/rss/yazarlar", TableCategory.HURRIYET, TableSubCategory.WRITER);
        addRow("http://www.hurriyet.com.tr/rss/magazin", TableCategory.HURRIYET, TableSubCategory.MAGAZINE);

        // En Son Haber
        addRow("https://www.ensonhaber.com/rss/ensonhaber.xml", TableCategory.ENSONHABER, TableSubCategory.HOME);
        addRow("https://www.ensonhaber.com/rss/mansetler.xml", TableCategory.ENSONHABER, TableSubCategory.BREAKING_NEWS);
        addRow("https://www.ensonhaber.com/rss/gundem.xml", TableCategory.ENSONHABER, TableSubCategory.HOME);
        addRow("https://www.ensonhaber.com/rss/ekonomi.xml", TableCategory.ENSONHABER, TableSubCategory.ECONOMY);
        addRow("https://www.ensonhaber.com/rss/dunya.xml", TableCategory.ENSONHABER, TableSubCategory.WORLD);
        addRow("https://www.ensonhaber.com/rss/saglik.xml", TableCategory.ENSONHABER, TableSubCategory.HEALT);
        addRow("https://www.ensonhaber.com/rss/otomobil.xml", TableCategory.ENSONHABER, TableSubCategory.CAR);
        addRow("https://www.ensonhaber.com/rss/teknoloji.xml", TableCategory.ENSONHABER, TableSubCategory.TECHNOLOGY);
        addRow("https://www.ensonhaber.com/rss/yasam.xml", TableCategory.ENSONHABER, TableSubCategory.LIFE);
        addRow("https://www.ensonhaber.com/rss/spor.xml", TableCategory.ENSONHABER, TableSubCategory.SPORT);
        addRow("https://www.ensonhaber.com/rss/magazin.xml", TableCategory.ENSONHABER, TableSubCategory.MAGAZINE);

        // Chip
        addRow("https://servis.chip.com.tr/chiponline.xml", TableCategory.CHIP, TableSubCategory.HOME);
        addRow("https://servis.chip.com.tr/chiponline-inceleme.xml", TableCategory.CHIP, TableSubCategory.TECHNOLOGY);
        addRow("https://servis.chip.com.tr/chiponline-haber.xml", TableCategory.CHIP, TableSubCategory.TECHNOLOGY);
        addRow("https://servis.chip.com.tr/chiponline-galeri.xml", TableCategory.CHIP, TableSubCategory.TECHNOLOGY);
        addRow("https://servis.chip.com.tr/chiponline-forum.xml", TableCategory.CHIP, TableSubCategory.TECHNOLOGY);

        // Star
        addRow("http://www.star.com.tr/rss/rss.asp", TableCategory.STAR, TableSubCategory.HOME);
        addRow("http://www.star.com.tr/rss/rss.asp?cid=500", TableCategory.STAR, TableSubCategory.BREAKING_NEWS);
//        addRow("http://www.star.com.tr/rss/yazarlarbugun.asp", TableCategory.STAR, TableSubCategory.WRITER);
        addRow("http://www.star.com.tr/rss/rss.asp?cid=13", TableCategory.STAR, TableSubCategory.BREAKING_NEWS);
        addRow("http://www.star.com.tr/rss/rss.asp?cid=15", TableCategory.STAR, TableSubCategory.ECONOMY);
        addRow("http://www.star.com.tr/rss/rss.asp?cid=16", TableCategory.STAR, TableSubCategory.SPORT);
        addRow("http://www.star.com.tr/rss/rss.asp?cid=17", TableCategory.STAR, TableSubCategory.WORLD);
        addRow("http://www.star.com.tr/rss/rss.asp?cid=20", TableCategory.STAR, TableSubCategory.MAGAZINE);
        addRow("http://www.star.com.tr/rss/rss.asp?cid=125", TableCategory.STAR, TableSubCategory.HEALT);
        addRow("http://www.star.com.tr/rss/rss.asp?cid=124", TableCategory.STAR, TableSubCategory.TECHNOLOGY);
        addRow("http://www.star.com.tr/rss/rss.asp?cid=170", TableCategory.STAR, TableSubCategory.CAR);

        // mynet
        addRow("https://www.mynet.com/haber/rss/sondakika", TableCategory.MYNET, TableSubCategory.HOME);
        addRow("https://www.mynet.com/haber/rss/kategori/guncel/", TableCategory.MYNET, TableSubCategory.BREAKING_NEWS);
        addRow("https://www.mynet.com/haber/rss/kategori/teknoloji/", TableCategory.MYNET, TableSubCategory.TECHNOLOGY);
        addRow("https://www.mynet.com/haber/rss/kategori/dunya/", TableCategory.MYNET, TableSubCategory.WORLD);
        addRow("https://www.mynet.com/haber/rss/kategori/yasam/", TableCategory.MYNET, TableSubCategory.LIFE);
        addRow("https://www.mynet.com/haber/rss/kategori/saglik/", TableCategory.MYNET, TableSubCategory.HEALT);
        addRow("https://www.mynet.com/haber/rss/kategori/spor/", TableCategory.MYNET, TableSubCategory.SPORT);
        addRow("https://www.mynet.com/haber/rss/kategori/sinema/", TableCategory.MYNET, TableSubCategory.ART_AND_CUSTURE);
        addRow("https://www.mynet.com/haber/rss/kategori/magazin/", TableCategory.MYNET, TableSubCategory.MAGAZINE);

        // Star
        addRow("https://www.sabah.com.tr/rss/anasayfa.xml", TableCategory.SABAH, TableSubCategory.HOME);
        addRow("https://www.sabah.com.tr/rss/gundem.xml", TableCategory.SABAH, TableSubCategory.BREAKING_NEWS);
        addRow("https://www.sabah.com.tr/rss/ekonomi.xml", TableCategory.SABAH, TableSubCategory.ECONOMY);
        addRow("https://www.sabah.com.tr/rss/spor.xml", TableCategory.SABAH, TableSubCategory.SPORT);
        addRow("https://www.sabah.com.tr/rss/yasam.xml", TableCategory.SABAH, TableSubCategory.LIFE);
        addRow("https://www.sabah.com.tr/rss/dunya.xml", TableCategory.SABAH, TableSubCategory.WORLD);
        addRow("https://www.sabah.com.tr/rss/teknoloji.xml", TableCategory.SABAH, TableSubCategory.TECHNOLOGY);
        addRow("https://www.sabah.com.tr/rss/otomobil.xml", TableCategory.SABAH, TableSubCategory.CAR);
        addRow("https://www.sabah.com.tr/rss/yazarlar.xml", TableCategory.SABAH, TableSubCategory.WRITER);
        addRow("https://www.sabah.com.tr/rss/saglik.xml", TableCategory.SABAH, TableSubCategory.HEALT);
        addRow("https://www.sabah.com.tr/rss/sondakika.xml", TableCategory.SABAH, TableSubCategory.BREAKING_NEWS);
        addRow("https://www.sabah.com.tr/rss/kultur-sanat.xml", TableCategory.SABAH, TableSubCategory.ART_AND_CUSTURE);

        // A Haber
        addRow("https://www.ahaber.com.tr/rss/anasayfa.xml", TableCategory.AHABER, TableSubCategory.HOME);
        addRow("https://www.ahaber.com.tr/rss/gundem.xml", TableCategory.AHABER, TableSubCategory.BREAKING_NEWS);
        addRow("https://www.ahaber.com.tr/rss/ekonomi.xml", TableCategory.AHABER, TableSubCategory.ECONOMY);
        addRow("https://www.ahaber.com.tr/rss/spor.xml", TableCategory.AHABER, TableSubCategory.SPORT);
        addRow("https://www.ahaber.com.tr/rss/yasam.xml", TableCategory.AHABER, TableSubCategory.LIFE);
        addRow("https://www.ahaber.com.tr/rss/dunya.xml", TableCategory.AHABER, TableSubCategory.WORLD);
        addRow("https://www.ahaber.com.tr/rss/teknoloji.xml", TableCategory.AHABER, TableSubCategory.TECHNOLOGY);
        addRow("https://www.ahaber.com.tr/rss/magazin.xml", TableCategory.AHABER, TableSubCategory.MAGAZINE);
        addRow("https://www.ahaber.com.tr/rss/saglik.xml", TableCategory.AHABER, TableSubCategory.HEALT);
        addRow("https://www.ahaber.com.tr/rss/yazarlar.xml", TableCategory.AHABER, TableSubCategory.WRITER);

        // Milliyet
        addRow("http://www.milliyet.com.tr/rss/rssNew/gundemRss.xml", TableCategory.MILLIYET, TableSubCategory.HOME);
        addRow("http://www.milliyet.com.tr/rss/rssNew/ekonomiRss.xml", TableCategory.MILLIYET, TableSubCategory.ECONOMY);
        addRow("http://www.milliyet.com.tr/rss/rssNew/magazinRss.xml", TableCategory.MILLIYET, TableSubCategory.MAGAZINE);
        addRow("http://www.milliyet.com.tr/rss/rssNew/gundemRss.xml", TableCategory.MILLIYET, TableSubCategory.BREAKING_NEWS);
        addRow("http://www.milliyet.com.tr/rss/rssNew/otomobilRss.xml", TableCategory.MILLIYET, TableSubCategory.CAR);
        addRow("http://www.milliyet.com.tr/rss/rssNew/teknolojiRss.xml", TableCategory.MILLIYET, TableSubCategory.TECHNOLOGY);
        addRow("http://www.milliyet.com.tr/rss/rssNew/saglikRss.xml", TableCategory.MILLIYET, TableSubCategory.HEALT);
        addRow("http://www.milliyet.com.tr/rss/rssNew/diyetRss.xml", TableCategory.MILLIYET, TableSubCategory.HEALT);
        addRow("http://www.milliyet.com.tr/rss/rssNew/yasamRss.xml", TableCategory.MILLIYET, TableSubCategory.LIFE);
        addRow("http://www.milliyet.com.tr/rss/rssNew/SonDakikaRss.xml", TableCategory.MILLIYET, TableSubCategory.HOME);
        addRow("http://www.milliyet.com.tr/rss/rssNew/YazarlarRss.xml", TableCategory.MILLIYET, TableSubCategory.WRITER);
    }
}
