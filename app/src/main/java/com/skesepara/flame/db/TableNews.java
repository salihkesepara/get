package com.skesepara.flame.db;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.skesepara.flame.recyclerview.NewsItem;
import com.skesepara.flame.slider.SliderItem;

import java.util.ArrayList;
import java.util.List;

public class TableNews {
    private static final String TAG = TableNews.class.getSimpleName();
    private static final String LIMIT_SLIDER = "20";
    private static final String LIMIT_RECYCYLER_VIEW = "100";

    /* Table name */
    private final String TABLE_NAME = "news";

    /* Table rows */
    private final String ID = "id";
    private final String LINK = "link";
    private final String TITLE = "title";
    private final String DESCRIPTION = "description";
    private final String PUB_DATE = "pubDate";
    private final String IMAGE = "image";
    private final String CATEGORY_ID = "categoryId";
    private final String SUB_CATEGORY_ID = "subCategoryId";

    /* Our DbAdapter object */
    private DbAdapter mDbAdapter;

    public TableNews(DbAdapter dbAdapter) {
        mDbAdapter = dbAdapter;
    }

    /* Create table query */
    public final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "("
            + ID + " INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE, "
            + LINK+ " TEXT, "
            + TITLE+ " TEXT UNIQUE, "
            + DESCRIPTION+ " TEXT, "
            + PUB_DATE+ " TEXT, "
            + IMAGE+ " TEXT, "
            + CATEGORY_ID+ " TEXT, "
            + SUB_CATEGORY_ID+ " TEXT) ";

    /* Drop table query */
    public final String DROP_TABLE = "DROP TABLE " + TABLE_NAME;

    /* Delete table query */
    public final String DELETE_TABLE = "DELETE FROM " + TABLE_NAME;
    public final String TIMEOUT_DAY = "-3 days";
    public final String CLEAR_OLD_NEWS = "DELETE FROM "+ TABLE_NAME +" where "+ PUB_DATE +" < datetime('now', 'localtime', '"+ TIMEOUT_DAY +"')";

    /**
     * Add new row to TABLE
     * @param link news link
     * @param title news title
     * @param description news description
     * @param image news image
     * @param categoryId news category
     * @param subCategoryId news subCategory
     */
    public void addRow(String link, String title, String description, String pubDate, String image, String categoryId, String subCategoryId) {
        SQLiteDatabase db = mDbAdapter.getWritableDatabase();
        try {
            String sql = "insert into "+ TABLE_NAME +"("+ LINK +", "+ TITLE +", "+ DESCRIPTION +", "+ PUB_DATE +", "+ IMAGE +", "+ CATEGORY_ID +", "+ SUB_CATEGORY_ID +") " +
                    "select ?, ?, ?, ?, ?, ?, ? " +
                    "where not exists(select 1 from "+ TABLE_NAME +" where "+ LINK +" = ? OR "+ TITLE +" = ?)";
            String[] bindArgs = new String[]{
                    link, title, description, pubDate, image, categoryId, subCategoryId, link, title};

            db.execSQL(sql, bindArgs);
        } catch (Exception e) {
            Log.e(TAG, "getData: " + e.getMessage());
        } finally {
            db.close();
        }
    }

    public List<SliderItem> getSliderData() {
        SQLiteDatabase db = mDbAdapter.getReadableDatabase();
        List<SliderItem> sliderItemList = new ArrayList<>();

        try {
            String sql = "select news."+ ID +", news."+ TITLE +", news."+ IMAGE +", news."+ LINK +" " +
                    "from news " +
                    "inner join category on news."+ CATEGORY_ID +" = category.id " +
                    "inner join subCategory on news."+ SUB_CATEGORY_ID +" = subCategory.id " +
                    "where category.active = '1' and subCategory.active = '1' order by datetime(pubDate) desc limit ?";
            String[] selectionArgs = {LIMIT_SLIDER};

            Cursor cursor = db.rawQuery(sql, selectionArgs);
            Log.v(TAG, TABLE_NAME + ": getSliderData: " + Integer.toString(cursor.getCount()));

            while (cursor.moveToNext()) {
                sliderItemList.add(new SliderItem(
                        Integer.parseInt(cursor.getString(0)),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3)
                ));
            }
        } catch (Exception e) {
            Log.e(TAG, "getSliderData: " + e.getMessage());
        }

        db.close();

        return sliderItemList;
    }

    public ArrayList<NewsItem> getData() {
        SQLiteDatabase db = mDbAdapter.getReadableDatabase();
        ArrayList<NewsItem> newsItemList = new ArrayList<>();

        try {
            String sql = "select news."+ ID +", news."+ TITLE +", news."+ IMAGE +", news."+ DESCRIPTION +", news."+ PUB_DATE +", category.name, subCategory.name, news."+ LINK +", category.img " +
                    "from news " +
                    "inner join category on news."+ CATEGORY_ID +" = category.id " +
                    "inner join subCategory on news."+ SUB_CATEGORY_ID +" = subCategory.id " +
                    "where category.active = '1' and subCategory.active = '1' order by datetime(pubDate) desc limit ? offset ?";
            String[] selectionArgs = {LIMIT_RECYCYLER_VIEW, LIMIT_SLIDER};

            Cursor cursor = db.rawQuery(sql, selectionArgs);
            Log.v(TAG, TABLE_NAME + ": getData: " + Integer.toString(cursor.getCount()));

            while (cursor.moveToNext()) {
                newsItemList.add(new NewsItem(
                        Integer.parseInt(cursor.getString(0)),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3),
                        cursor.getString(4),
                        cursor.getString(5),
                        cursor.getString(6),
                        cursor.getString(7),
                        cursor.getString(8)
                ));
            }
        } catch (Exception e) {
            Log.e(TAG, "getData: " + e.getMessage());
        }

        db.close();

        return newsItemList;
    }

    public ArrayList<NewsItem> getFullData() {
        SQLiteDatabase db = mDbAdapter.getReadableDatabase();
        ArrayList<NewsItem> newsItemList = new ArrayList<>();

        try {
            String sql = "select news."+ ID +", news."+ TITLE +", news."+ IMAGE +", news."+ DESCRIPTION +", news."+ PUB_DATE +", category.name, subCategory.name, news."+ LINK +", category.img " +
                    "from news " +
                    "inner join category on news."+ CATEGORY_ID +" = category.id " +
                    "inner join subCategory on news."+ SUB_CATEGORY_ID +" = subCategory.id " +
                    "where category.active = '1' and subCategory.active = '1' order by datetime(pubDate) desc";
            String[] selectionArgs = {};

            Cursor cursor = db.rawQuery(sql, selectionArgs);
            Log.v(TAG, TABLE_NAME + ": getData: " + Integer.toString(cursor.getCount()));

            while (cursor.moveToNext()) {
                newsItemList.add(new NewsItem(
                        Integer.parseInt(cursor.getString(0)),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3),
                        cursor.getString(4),
                        cursor.getString(5),
                        cursor.getString(6),
                        cursor.getString(7),
                        cursor.getString(8)
                ));
            }
        } catch (Exception e) {
            Log.e(TAG, "getData: " + e.getMessage());
        }

        db.close();

        return newsItemList;
    }

    public void deleteTable() {
        SQLiteDatabase db = mDbAdapter.getWritableDatabase();
        try {
            db.execSQL(DELETE_TABLE);
        } catch (Exception e) {
            Log.e(TAG, "deleteTable: " + e.getMessage());
        }

        db.close();
    }

    public void clearOldNews() {
        SQLiteDatabase db = mDbAdapter.getWritableDatabase();
        try {
            db.execSQL(CLEAR_OLD_NEWS);
        } catch (Exception e) {
            Log.e(TAG, "clearOldNews: " + e.getMessage());
        }

        db.close();
    }
}
