package com.skesepara.flame.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.skesepara.flame.R;
import com.skesepara.flame.listview.CategoryItem;

import java.util.ArrayList;

public class TableCategory {
    private static final String TAG = TableCategory.class.getSimpleName();
    private static final String[] PATTERNS = {
            "EEE, dd MMM yyyy HH:mm:ss zzz", // Milliyet, Yenisafak, CNN
            "EEE, dd MMM yyyy HH:mm:ss 'Z'", // Hurriyet
            "yyyy-MM-dd'T'HH:mm:ss'+03:00'", // NTV
            "EEE, dd MMM yyyy HH:mm:ss '+03:00'", // Sabah
            "EEE, dd MMM yyyy HH:mm:ss '+0300'", // En son haber
            "EEE, dd MMM yyyy HH:mm:ss 'UT'" // En son haber
    };

    /* Id */
    public static final String CNN = "1";
    public static final String YENISAFAK = "2";
    public static final String HURRIYET = "3";
    public static final String ENSONHABER = "4";
    public static final String CHIP = "5";
    public static final String STAR = "6";
    public static final String MYNET = "7";
    public static final String SABAH = "8";
    public static final String AHABER = "9";
    public static final String MILLIYET = "10";

    /* Table name */
    private final String TABLE_NAME = "category";

    /* Table rows */
    private final String ID = "id";
    private final String NAME = "name";
    private final String IMG = "img";
    private final String PATTERN = "pattern";
    private final String ACTIVE = "active";

    /* Our DbAdapter object */
    private DbAdapter mDbAdapter;

    /* MainActivity Context data */
    private Context mContext;

    public TableCategory(Context context, DbAdapter dbAdapter) {
        mDbAdapter = dbAdapter;
        mContext = context;
    }

    /* Create table query */
    public final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "("
            + ID + " TEXT PRIMARY KEY UNIQUE, "
            + NAME+ " TEXT UNIQUE, "
            + IMG+ " TEXT, "
            + PATTERN+ " TEXT, "
            + ACTIVE + " TEXT) ";

    /* Drop table query */
    public final String DROP_TABLE = "DROP TABLE " + TABLE_NAME;

    public void addRow(String id, String name, String img, String pattern, String active) {
        SQLiteDatabase db = mDbAdapter.getWritableDatabase();
        try {
            String sql = "insert into "+ TABLE_NAME +"("+ ID +", "+ NAME +", "+ IMG +", "+ PATTERN +", "+ ACTIVE +") " +
                    "select ?, ?, ?, ?, ? " +
                    "where not exists(select 1 from "+ TABLE_NAME +" where "+ NAME +" = ?)";
            String[] bindArgs = {id, name, img, pattern, active, name};

            db.execSQL(sql, bindArgs);
        } catch (Exception e) {
            Log.e(TAG, "addRow: " + e.getMessage());
        }

        db.close();
    }

    public ArrayList<CategoryItem> getData() {
        SQLiteDatabase db = mDbAdapter.getReadableDatabase();
        ArrayList<CategoryItem> categoryList = new ArrayList<>();
        String orderBy = "name ASC";

        try {
            String[] columns = {ID, NAME, IMG, PATTERN, ACTIVE};
            Cursor cursor = db.query(TABLE_NAME, columns, null, null, null, null, orderBy);
            Log.v(TABLE_NAME, Integer.toString(cursor.getCount()));
            while (cursor.moveToNext()) {
                String id = cursor.getString(0);
                String name = cursor.getString(1);
                String img = cursor.getString(2);
                String pattern = cursor.getString(3);
                String active = cursor.getString(4);
                categoryList.add(new CategoryItem(id, name, img, pattern, active));
            }
        } catch (Exception e) {
            Log.e(TAG, "getData: " + e.getMessage());
        }

        db.close();
        return categoryList;
    }

    public int getActiveSize() {
        SQLiteDatabase db = mDbAdapter.getReadableDatabase();
        int activeSize = -1;

        try {
            String sql = "SELECT COUNT("+ ID +") FROM "+ TABLE_NAME +" WHERE "+ ACTIVE +" = ?";
            String[] selectionArgs = {"1"};

            Cursor cursor = db.rawQuery(sql, selectionArgs);
            cursor.moveToNext();
            return Integer.parseInt(cursor.getString(0));

        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }

        db.close();
        return activeSize;
    }

    public void fillTable() {
        addRow(CNN, mContext.getResources().getString(R.string.cnn_turk), String.valueOf(R.drawable.cnn), PATTERNS[0], "0");
        addRow(YENISAFAK, mContext.getResources().getString(R.string.yenisafak), String.valueOf(R.drawable.yenisafak), PATTERNS[0], "0");
        addRow(HURRIYET, mContext.getResources().getString(R.string.hurriyet), String.valueOf(R.drawable.hurriyet), PATTERNS[1], "0");
        addRow(ENSONHABER, mContext.getResources().getString(R.string.ensonhaber), String.valueOf(R.drawable.ensonhaber), PATTERNS[4], "0");
        addRow(CHIP, mContext.getResources().getString(R.string.chip), String.valueOf(R.drawable.chip), PATTERNS[5], "1");
        addRow(STAR, mContext.getResources().getString(R.string.star), String.valueOf(R.drawable.star), PATTERNS[0], "0");
        addRow(MYNET, mContext.getResources().getString(R.string.mynet), String.valueOf(R.drawable.mynet), PATTERNS[4], "0");
        addRow(SABAH, mContext.getResources().getString(R.string.sabah), String.valueOf(R.drawable.sabah), PATTERNS[0], "0");
        addRow(AHABER, mContext.getResources().getString(R.string.a_haber), String.valueOf(R.drawable.a_haber), PATTERNS[0], "0");
//        addRow(MILLIYET, mContext.getResources().getString(R.string.milliyet), String.valueOf(R.drawable.milliyet), PATTERNS[0], "1");
    }

    public void updateActive(String id, String active) {
        SQLiteDatabase db = mDbAdapter.getWritableDatabase();
        ContentValues cv = new ContentValues();
        try {
            cv.put(ACTIVE, active);
            String whereClause = ID+"="+id;
            db.update(TABLE_NAME, cv, whereClause, null);
        } catch (Exception e) {
            Log.e(TAG, "updateActive: " + e.getMessage());
        }

        db.close();
    }
}
