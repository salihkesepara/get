package com.skesepara.flame.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

public class DbAdapter extends SQLiteOpenHelper {
    private static final String TAG = DbAdapter.class.getSimpleName();
    /* Database name */
    private static final String DB_NAME = "news.db";

    /* Database version */
    private static final int DB_VERSION = 1;

    /* MainActivity Context data */
    private Context mContext;

    public DbAdapter(@Nullable Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createTables(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        dropTables(db);
        createTables(db);
    }

    private void createTables(SQLiteDatabase db) {
        TableCategory tableCategory = new TableCategory(mContext, this);
        TableLink tableLink = new TableLink(this);
        TableNews tableNews = new TableNews(this);
        TableSubCategory tableSubCategory = new TableSubCategory(mContext, this);
        TableSettings tableSettings = new TableSettings(this);

        db.execSQL(tableNews.CREATE_TABLE);
        db.execSQL(tableCategory.CREATE_TABLE);
        db.execSQL(tableSubCategory.CREATE_TABLE);
        db.execSQL(tableLink.CREATE_TABLE);
        db.execSQL(tableSettings.CREATE_TABLE);
    }

    private void dropTables(SQLiteDatabase db) {
        TableCategory tableCategory = new TableCategory(mContext, this);
        TableLink tableLink = new TableLink(this);
        TableNews tableNews = new TableNews(this);
        TableSubCategory tableSubCategory = new TableSubCategory(mContext, this);
        TableSettings tableSettings = new TableSettings(this);

        db.execSQL(tableNews.DROP_TABLE);
        db.execSQL(tableCategory.DROP_TABLE);
        db.execSQL(tableSubCategory.DROP_TABLE);
        db.execSQL(tableLink.DROP_TABLE);
        db.execSQL(tableSettings.DROP_TABLE);
    }
}
