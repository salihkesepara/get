package com.skesepara.flame.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.skesepara.flame.R;
import java.util.ArrayList;
import com.skesepara.flame.listview.SubCategoryItem;

public class TableSubCategory {
    private static final String TAG = TableSubCategory.class.getSimpleName();

    /* Id */
    public static final String BREAKING_NEWS = "1";
    public static final String WORLD = "2";
    public static final String ART_AND_CUSTURE = "3";
    public static final String TECHNOLOGY = "4";
    public static final String CAR = "5";
    public static final String ECONOMY = "6";
    public static final String HEALT = "7";
    public static final String SPORT = "8";
    public static final String TRAVEL = "9";
    public static final String WRITER = "10";
    public static final String LIFE = "11";
    public static final String HOME = "12";
    public static final String MAGAZINE = "13";

    /* Table name */
    private final String TABLE_NAME = "subCategory";

    /* Table rows */
    private final String ID = "id";
    private final String NAME = "name";
    private final String IMG = "image";
    private final String ACTIVE = "active";

    /* Our DbAdapter object */
    private DbAdapter mDbAdapter;

    /* MainActivity Context data */
    private Context mContext;

    public TableSubCategory(Context context, DbAdapter dbAdapter) {
        mDbAdapter = dbAdapter;
        mContext = context;
    }

    /* Create table query */
    public final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "("
            + ID + " INTEGER PRIMARY KEY UNIQUE, "
            + NAME+ " TEXT UNIQUE, "
            + IMG+ " TEXT, "
            + ACTIVE + " TEXT) ";

    /* Drop table query */
    public final String DROP_TABLE = "DROP TABLE " + TABLE_NAME;

    public void addRow(String id, String name, String img, String active) {
        SQLiteDatabase db = mDbAdapter.getWritableDatabase();
        try {
            String sql = "insert into "+ TABLE_NAME +"("+ ID +","+ NAME +", "+ IMG +", "+ ACTIVE +") " +
                    "select ?, ?, ?, ? " +
                    "where not exists(select 1 from "+ TABLE_NAME +" where "+ NAME +" = ?)";
            String[] bindArgs = {id, name, img, active, name};

            db.execSQL(sql, bindArgs);
        } catch (Exception e) {
            Log.e(TAG, "addRow: " + e.getMessage());
        }

        db.close();
    }

    public ArrayList<SubCategoryItem> getData() {
        SQLiteDatabase db = mDbAdapter.getReadableDatabase();
        ArrayList<SubCategoryItem> subCategoryList = new ArrayList<>();
        try {
            String sql = "select distinct subCategory.id, subCategory.name, subCategory.image, subCategory.active from link " +
                    "join category on link.categoryId = category.id " +
                    "join subCategory on link.subCategoryId = subCategory.id " +
                    "where category.active = '1' " +
                    "order by subCategory.name asc";
            Cursor cursor = db.rawQuery(sql, null);
            Log.v(TABLE_NAME, Integer.toString(cursor.getCount()));
            while (cursor.moveToNext()) {
                String id = cursor.getString(0);
                String name = cursor.getString(1);
                String img = cursor.getString(2);
                String active = cursor.getString(3);
                subCategoryList.add(new SubCategoryItem(id, name, img, active));
            }
        } catch (Exception e) {
            Log.e(TAG, "getData: " + e.getMessage());
        }

        db.close();
        return subCategoryList;
    }

    public ArrayList<SubCategoryItem> getAllSubCategory() {
        SQLiteDatabase db = mDbAdapter.getReadableDatabase();
        ArrayList<SubCategoryItem> subCategoryList = new ArrayList<>();
        try {
            String sql = "select "+ ID +", "+ NAME +", "+ IMG +", "+ ACTIVE +" from "+ TABLE_NAME +"";
            Cursor cursor = db.rawQuery(sql, null);
            Log.v(TABLE_NAME, Integer.toString(cursor.getCount()));
            while (cursor.moveToNext()) {
                String id = cursor.getString(0);
                String name = cursor.getString(1);
                String img = cursor.getString(2);
                String active = cursor.getString(3);
                subCategoryList.add(new SubCategoryItem(id, name, img, active));
            }
        } catch (Exception e) {
            Log.e(TAG, "getAllSubCategory: " + e.getMessage());
        }

        db.close();
        return subCategoryList;
    }

    public int getActiveSize() {
        SQLiteDatabase db = mDbAdapter.getReadableDatabase();
        int activeSize = -1;

        try {
            String sql = "select count(distinct subCategory.id) from link " +
                    "join category on link.categoryId = category.id " +
                    "join subCategory on link.subCategoryId = subCategory.id " +
                    "where subCategory.active = '1' and category.active = '1'";

            Cursor cursor = db.rawQuery(sql, null);
            cursor.moveToNext();
            return Integer.parseInt(cursor.getString(0));

        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }

        db.close();
        return activeSize;
    }

    public void fillTable() {
        addRow(HOME, mContext.getResources().getString(R.string.home), String.valueOf(R.drawable.home),"1");
        addRow(BREAKING_NEWS, mContext.getResources().getString(R.string.breaking_news), String.valueOf(R.drawable.breaking_news), "0");
        addRow(WORLD, mContext.getResources().getString(R.string.world), String.valueOf(R.drawable.world), "0");
        addRow(ART_AND_CUSTURE, mContext.getResources().getString(R.string.art_and_culture), String.valueOf(R.drawable.art), "0");
        addRow(TECHNOLOGY, mContext.getResources().getString(R.string.technology), String.valueOf(R.drawable.tech), "0");
        addRow(CAR, mContext.getResources().getString(R.string.car), String.valueOf(R.drawable.car),"0");
        addRow(ECONOMY, mContext.getResources().getString(R.string.economy), String.valueOf(R.drawable.economy),"0");
        addRow(HEALT, mContext.getResources().getString(R.string.healt), String.valueOf(R.drawable.healt),"0");
        addRow(SPORT, mContext.getResources().getString(R.string.sport), String.valueOf(R.drawable.sport),"0");
        addRow(TRAVEL, mContext.getResources().getString(R.string.travel), String.valueOf(R.drawable.travel),"0");
        addRow(WRITER, mContext.getResources().getString(R.string.writer), String.valueOf(R.drawable.writer),"0");
        addRow(LIFE, mContext.getResources().getString(R.string.life), String.valueOf(R.drawable.life),"0");
        addRow(MAGAZINE, mContext.getResources().getString(R.string.magazine), String.valueOf(R.drawable.magazine),"0");
    }

    public void updateActive(String id, String active) {
        SQLiteDatabase db = mDbAdapter.getWritableDatabase();
        ContentValues cv = new ContentValues();

        try {
            cv.put(ACTIVE, active);
            String whereClause = ID+"="+id;
            db.update(TABLE_NAME, cv, whereClause, null);
        } catch (Exception e) {
            Log.e(TAG, "updateActive: " + e.getMessage());
        }

        db.close();
    }
}
