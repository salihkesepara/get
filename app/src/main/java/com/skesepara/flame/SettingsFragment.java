package com.skesepara.flame;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;

import com.skesepara.flame.db.DbAdapter;
import com.skesepara.flame.db.TableSettings;

public class SettingsFragment extends Fragment {
    private static final String TAG = MainActivity.class.getSimpleName();

    private Switch switchDontDownloadImage;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        DbAdapter dbAdapter = new DbAdapter(getContext());
        final TableSettings tableSettings = new TableSettings(dbAdapter);
        switchDontDownloadImage = getActivity().findViewById(R.id.switchDontDownloadImage);
        String isImageDownloadable = tableSettings.getIsImageDownloadable();

        switchDontDownloadImage.setChecked(isImageDownloadable.equals(TableSettings.IMAGE_NOT_DOWNLOADABLE));
        switchDontDownloadImage.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    tableSettings.updateDownloadImageRule(TableSettings.IMAGE_NOT_DOWNLOADABLE);
                } else {
                    tableSettings.updateDownloadImageRule(TableSettings.IMAGE_DOWNLOADABLE);
                }
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_settings, container, false);
    }
}
