package com.skesepara.flame.dialogs;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import com.skesepara.flame.R;
import com.skesepara.flame.fragments.FragmentCategory;
import com.skesepara.flame.fragments.FragmentSubCategory;
import com.skesepara.flame.fragments.ViewPagerAdapterSourceDialog;
import com.skesepara.flame.listview.CategoryItem;
import com.skesepara.flame.listview.SubCategoryItem;

import java.util.ArrayList;

public class SourceDialog extends DialogFragment implements FragmentCategory.CategoryListener {
    private static final String TAG = SourceDialog.class.getSimpleName();

    public SourceDialogListener mSourceDialogListener;
    public SourceDialogCategoryListener mSourceDialogCategoryListener;

    /* Fragments */
    private FragmentCategory mFragmentCategory;
    private FragmentSubCategory mFragmentSubCategory;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_source, container, false);
        Button mButtonSourceDialogExit;
        Button mButtonDoneDialog;
        initTablayout(view);

        mButtonSourceDialogExit = view.findViewById(R.id.buttonSourceDialogExit);
        mButtonSourceDialogExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSourceDialogListener.sourceDialogOnCancel();
                getDialog().dismiss();
            }
        });

        mButtonDoneDialog = view.findViewById(R.id.buttonDoneDialog);
        mButtonDoneDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mFragmentCategory.getActiveSize() < FragmentCategory.MIN_CATEGORY_TO_SELECT) {
                    mFragmentCategory.showToast(R.string.min_category, FragmentCategory.MIN_CATEGORY_TO_SELECT);
                } else if (mFragmentSubCategory.getActiveSize() < FragmentSubCategory.MIN_SUB_CATEGORY_TO_SELECT) {
                    mFragmentSubCategory.showToast(R.string.min_sub_category, FragmentSubCategory.MIN_SUB_CATEGORY_TO_SELECT);
                } else {
                    ArrayList<CategoryItem> categoryItemList = mFragmentCategory.getCategoryItemList();
                    ArrayList<SubCategoryItem> subCategoryItemList = mFragmentSubCategory.getSubCategoryItemList();
                    mSourceDialogListener.sourceDialogOnDone(categoryItemList, subCategoryItemList);
                    getDialog().dismiss();
                }
            }
        });

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        // Get existing layout params for the window
        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
        // Assign window properties to fill the parent
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.MATCH_PARENT;


        getDialog().getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

    }

    public void initTablayout(View view) {
        TabLayout tabLayoutDialogSource = view.findViewById(R.id.tabLayoutDialogSource);
        ViewPager viewPagerDialogSource = view.findViewById(R.id.viewPagerDialogSource);

        ViewPagerAdapterSourceDialog adapter = new ViewPagerAdapterSourceDialog(getChildFragmentManager());

        /* Adding fragments */
        adapter.addFragment(mFragmentCategory, getResources().getString(R.string.category));
        adapter.addFragment(mFragmentSubCategory, getResources().getString(R.string.sub_category));

        /* Adapter setup */
        viewPagerDialogSource.setAdapter(adapter);
        tabLayoutDialogSource.setupWithViewPager(viewPagerDialogSource);
    }

    public interface SourceDialogListener {
        void sourceDialogOnDone(ArrayList<CategoryItem> categoryItemList, ArrayList<SubCategoryItem> subCategoryItemList);
        void sourceDialogOnCancel();
    }

    public interface SourceDialogCategoryListener {
        void categorySelected();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mFragmentCategory = new FragmentCategory();
        mFragmentSubCategory = new FragmentSubCategory();

        try {
            mSourceDialogListener = (SourceDialogListener) context;
            mSourceDialogCategoryListener = mFragmentSubCategory;
        } catch (ClassCastException e) {
            Log.e(TAG, "onAttach: ClassCastException" + e.getMessage());
            throw new ClassCastException(context.toString() + " must implement SourceDialogListener");
        }
    }

    @Override
    public void onCategorySelected() {
        mSourceDialogCategoryListener.categorySelected();
    }
}
