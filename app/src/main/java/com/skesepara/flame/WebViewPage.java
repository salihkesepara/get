package com.skesepara.flame;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.skesepara.flame.recyclerview.NewsAdapter;

public class WebViewPage extends AppCompatActivity {
    private static final String TAG = WebViewPage.class.getSimpleName();
    private WebView webViewNews;
    public static final String LINK = "LINK";
    private Context context = this;
    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view_page);

        webViewNews = findViewById(R.id.webViewNews);
        WebSettings webSettings = webViewNews.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        webViewNews.setWebViewClient(new WebViewClient());
        Intent intent = getIntent();
        String link = intent.getStringExtra(LINK);

        webViewNews.loadUrl(link);
        initInterstitialAd();
    }

    private void initInterstitialAd() {
        String adMobAppId = getResources().getString(R.string.ad_mob_app_id);
        MobileAds.initialize(context, adMobAppId);
        mInterstitialAd = new InterstitialAd(context);
        String adUnitId = getResources().getString(R.string.ad_unit_id_interstitial_test);
        mInterstitialAd.setAdUnitId(adUnitId);
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .build();
        mInterstitialAd.loadAd(adRequest);

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                Log.v(TAG, "onAdClosed");
                super.onAdClosed();
            }

            @Override
            public void onAdFailedToLoad(int i) {
                Log.e(TAG, "onAdFailedToLoad: " + String.valueOf(i));
                super.onAdFailedToLoad(i);
            }

            @Override
            public void onAdLeftApplication() {
                Log.v(TAG, "onAdLeftApplication");
                super.onAdLeftApplication();
            }

            @Override
            public void onAdOpened() {
                Log.v(TAG, "onAdOpened");
                super.onAdOpened();
            }

            @Override
            public void onAdLoaded() {
                Log.v(TAG, "onAdLoaded");
                super.onAdLoaded();
            }

            @Override
            public void onAdClicked() {
                Log.v(TAG, "onAdClicked");
                super.onAdClicked();
            }

            @Override
            public void onAdImpression() {
                super.onAdImpression();
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (DashboardFragment.ACTIVATED_INTERSTITIAL_ADS &&
                NewsAdapter.WEBVIEW_COUNTER != 0 &&
                NewsAdapter.WEBVIEW_COUNTER % DashboardFragment.ITEM_PER_INTERSTITIAL_AD == 0 &&
                mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }

        super.onBackPressed();
    }


}
