package com.skesepara.flame;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.skesepara.flame.db.DbAdapter;
import com.skesepara.flame.db.RssLinkItem;
import com.skesepara.flame.db.TableCategory;
import com.skesepara.flame.db.TableLink;
import com.skesepara.flame.db.TableNews;
import com.skesepara.flame.db.TableSettings;
import com.skesepara.flame.db.TableSubCategory;
import com.skesepara.flame.dialogs.SourceDialog;
import com.skesepara.flame.feeds.ReadRss;
import com.skesepara.flame.listview.CategoryItem;
import com.skesepara.flame.listview.SubCategoryItem;
import com.skesepara.flame.recyclerview.NewsAdapter;
import com.skesepara.flame.recyclerview.NewsItem;
import com.skesepara.flame.slider.SliderItem;
import com.skesepara.flame.utilities.Firebase;
import com.skesepara.flame.utilities.NetworkUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class DashboardFragment extends Fragment implements Firebase.FirebaseListener {
    private static final String TAG = DashboardFragment.class.getSimpleName();
    public static boolean ACTIVATED_INTERSTITIAL_ADS = false;
    public static int ITEM_PER_INTERSTITIAL_AD = 6;

    /* objects */
    private DbAdapter dbAdapter;
    private LinearLayout linearLayoutNoResult;
    private SwipeRefreshLayout swipeRefreshLayoutNews;
    private RecyclerView recyclerView;
    private TableCategory tableCategory;
    private TableSubCategory tableSubCategory;
    private TableLink tableLink;
    private TableNews tableNews;
    private TableSettings tableSettings;
    private FloatingActionButton fabAdd;
    private FloatingActionButton fabUp;
    public static NewsAdapter newsAdapter;
    public static int mTotalScrolled = 0;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initialiser();
    }

    public void initialiser() {
        linearLayoutNoResult = getActivity().findViewById(R.id.linearLayoutNoResult);
        initRecyclerView();
        initFabs();
        fillTables();
        initSwipeRefreshLayout();
        scrollListener();
        Firebase.build(this);
        loadRecyclerView();

        checkFirstOpen();
        checkSource();
    }

    public void checkSource() {
        if (tableCategory.getActiveSize() == 0 || tableSubCategory.getActiveSize() == 0) {
            showSourceDialog();
        }
    }

    public void checkFirstOpen() {
        String isFirstOpen = tableSettings.getFirstOpen();
        Log.v("isFirstOpen", isFirstOpen);

        if (isFirstOpen.equals(TableSettings.FIRST_OPEN)) {
            showSourceDialog();
        }

        tableSettings.updateFirstOpen(TableSettings.NOT_FIRST_OPEN);
    }

    public void initRecyclerView() {
        recyclerView = getActivity().findViewById(R.id.recyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
    }

    public void initFabs() {
        fabAdd = getActivity().findViewById(R.id.fabAdd);
        fabUp = getActivity().findViewById(R.id.fabTop);

        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSourceDialog();
            }
        });
        fabUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerView.smoothScrollToPosition(0);
                fabAdd.show();
            }
        });
    }

    public void showSourceDialog() {
        SourceDialog sourceDialog = new SourceDialog();

        Fragment fragment = getFragmentManager().findFragmentByTag("SourceDialog");

        if (fragment == null) {
            sourceDialog.show(getActivity().getSupportFragmentManager(), "SourceDialog");
        }
    }

    public void fillTables() {
        initTables();
        tableCategory.fillTable();
        tableSubCategory.fillTable();
        tableLink.fillTable();
        tableSettings.fillTable();
    }

    public void initTables() {
        dbAdapter = new DbAdapter(getContext());
        tableCategory = new TableCategory(getContext(), dbAdapter);
        tableSubCategory = new TableSubCategory(getContext(), dbAdapter);
        tableLink = new TableLink(dbAdapter);
        tableNews = new TableNews(dbAdapter);
        tableSettings = new TableSettings(dbAdapter);
    }

    public void loadRecyclerView() {
        clearSearchView();
        loadCache();
        resetControls();

        if (!NetworkUtils.isNetworkConnection()) {
            swipeRefreshLayoutNews.setRefreshing(false);
            return;
        }
        ArrayList<RssLinkItem> rssLinkItemList = tableLink.getData();
        if (rssLinkItemList.size() != 0) {
            linearLayoutNoResult.setVisibility(ImageView.INVISIBLE);
            ReadRss readRss = new ReadRss(getContext(), recyclerView, swipeRefreshLayoutNews,
                    linearLayoutNoResult, rssLinkItemList);
            readRss.fetch();
        } else {
            swipeRefreshLayoutNews.setRefreshing(false);
        }
    }

    public void resetControls() {
        fabAdd.show();
        fabUp.hide();
        mTotalScrolled = 0;
    }

    public void loadCache() {
        ArrayList<NewsItem> newsItemList = tableNews.getData();
        ArrayList<NewsItem> newsItemListFull = tableNews.getFullData();
        List<SliderItem> sliderItemList = tableNews.getSliderData();
        if (newsItemList.size() != 0 || sliderItemList.size() != 0) {
            linearLayoutNoResult.setVisibility(ImageView.INVISIBLE);
            newsAdapter = new NewsAdapter(getContext(), newsItemList, newsItemListFull, sliderItemList, tableSettings);
            recyclerView.setAdapter(newsAdapter);
        } else {
            recyclerView.setAdapter(null);
            linearLayoutNoResult.setVisibility(ImageView.VISIBLE);
        }
    }

    public void initSwipeRefreshLayout() {
        swipeRefreshLayoutNews = getActivity().findViewById(R.id.swipeRefreshLayoutNews);
        swipeRefreshLayoutNews.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadRecyclerView();
            }
        });
    }

    public void sourceDialogOnDone(ArrayList<CategoryItem> categoryItemList, ArrayList<SubCategoryItem> subCategoryItemList) {
        NetworkUtils.isNetworkConnection(getContext());
        loadRecyclerView();
    }

    public void clearSearchView(){
        if (MainActivity.searchView != null) {
            MainActivity.searchView.setQuery("", false);
            MainActivity.searchView.setIconified(true);
        }
    }

    public void sourceDialogOnCancel() {
        // Cancel Dialog
    }

    public void clearOldNews() {
        tableNews.clearOldNews();
    }

    public void scrollListener() {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                mTotalScrolled += dy;
                if (mTotalScrolled < 1000) {
                    fabUp.hide();
                    fabAdd.show();
                }
                if (dy > 0) {
                    if (fabAdd.getVisibility() == View.VISIBLE) {
                        fabAdd.hide();
                    }
                    if (fabUp.getVisibility() == View.VISIBLE) {
                        fabUp.hide();
                    }
                } else if (dy < 0 && mTotalScrolled > 1000) {
                    if (fabAdd.getVisibility() != View.VISIBLE) {
                        fabAdd.show();
                    }
                    if (fabUp.getVisibility() != View.VISIBLE) {
                        fabUp.show();
                    }
                }
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.v(TAG, "mTotalScrolled: " + mTotalScrolled);
        return inflater.inflate(R.layout.fragment_dashboard, container, false);
    }

    @Override
    public void onAttach(Context context) {
        Log.v(TAG, "onAttach");
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Log.v(TAG, "onAttach");
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        Log.v(TAG, "onAttach");
        super.onStart();
    }

    @Override
    public void onResume() {
        Log.v(TAG, "onAttach");
        super.onResume();
    }

    public void filterAdapter(String newText) {
        if (newsAdapter != null) {
            newsAdapter.getFilter().filter(newText);
        }
    }

    @Override
    public void adsOnChanged(DataSnapshot dataSnapshot) {
        DataSnapshot banner = dataSnapshot.child(Firebase.BANNER);
        DataSnapshot interstitial = dataSnapshot.child(Firebase.INTERSTITIAL);

        NewsAdapter.ACTIVATED_ADS = (Boolean) banner.child(Firebase.ACTIVATED_ADS).getValue();
        NewsAdapter.ITEMS_PER_AD = banner.child(Firebase.ITEM_PER_AD).getValue(Integer.class);

        ACTIVATED_INTERSTITIAL_ADS = (Boolean) interstitial.child(Firebase.ACTIVATED_ADS).getValue();
        ITEM_PER_INTERSTITIAL_AD = interstitial.child(Firebase.ITEM_PER_AD).getValue(Integer.class);
    }
}
