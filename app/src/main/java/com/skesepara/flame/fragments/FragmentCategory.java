package com.skesepara.flame.fragments;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.skesepara.flame.R;
import com.skesepara.flame.db.DbAdapter;
import com.skesepara.flame.db.TableCategory;
import com.skesepara.flame.db.TableSubCategory;
import com.skesepara.flame.listview.CategoryAdapter;
import com.skesepara.flame.listview.CategoryItem;
import com.skesepara.flame.listview.SubCategoryItem;

import java.util.ArrayList;
import java.util.Objects;

public class FragmentCategory extends Fragment {
    private static final String TAG = FragmentCategory.class.getSimpleName();
    public final static String ACTIVE = "1";
    public final static String PASSIVE = "0";
    public final static int EMPTY_IMAGE = 0;
    private final static int MAX_CATEGORY_TO_SELECT = 5;
    public final static int MIN_CATEGORY_TO_SELECT = 1;
    private CategoryListener mCategoryListener;
    private TableCategory mTableCategory;
    private TableSubCategory mTableSubCategory;

    /* objects */
    private View view;
    private ArrayList<CategoryItem> mCategoryItemList;

    /* variables */
    int activeSize = -1;

    public FragmentCategory() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_category, container, false);
        initListCategory(view);
        return view;
    }

    public void initListCategory(View view) {
        ListView listViewCategory = view.findViewById(R.id.listViewCategoryDialog);

        /* We need dbAdapter for getting the tableCategory methods */
        DbAdapter dbAdapter = new DbAdapter(getContext());

        /* Define a tableCategory object for call the getData() method */
        mTableCategory = new TableCategory(getContext(), dbAdapter);

        /* Define a tableSubCategory object */
        mTableSubCategory = new TableSubCategory(getContext(), dbAdapter);

        /* Get Category's all data */
        mCategoryItemList = mTableCategory.getData();

        /* Define categoryAdapter for listView */
        CategoryAdapter categoryAdapter = new CategoryAdapter(getContext(), mCategoryItemList);

        /* setAdapter into listView */
        listViewCategory.setAdapter(categoryAdapter);

        activeSize = getActiveSize();

        listViewCategory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ImageView img = view.findViewById(R.id.imageViewCheck);
                if (Objects.equals(mCategoryItemList.get(position).getActive(), ACTIVE)) {
                    mTableCategory.updateActive(mCategoryItemList.get(position).getId(), PASSIVE);
                    mCategoryItemList.get(position).setActive(PASSIVE);
                    img.setImageResource(EMPTY_IMAGE);
                    mCategoryListener.onCategorySelected();
                    activeSize = activeSize - 1;

                    /* Get active this line if necessary */
//                    checkForSubCategoryActive();
                } else {
                    if (isSelectable()) {
                        mTableCategory.updateActive(mCategoryItemList.get(position).getId(), ACTIVE);
                        mCategoryItemList.get(position).setActive(ACTIVE);
                        img.setImageResource(R.drawable.ic_check_list);
                        mCategoryListener.onCategorySelected();
                    }
                }
            }
        });
    }

    public void checkForSubCategoryActive() {
        ArrayList<SubCategoryItem> subCategoryItemListAll = mTableSubCategory.getAllSubCategory();

        for (SubCategoryItem subCategoryItemAll : subCategoryItemListAll) {
            /* If the selected sub-category is passive, go to next. */
            if (subCategoryItemAll.getActive().equals(PASSIVE)) continue;

            if (setPassiveIfIsNotShowing(subCategoryItemAll)) {
                mTableSubCategory.updateActive(subCategoryItemAll.getId(), PASSIVE);
            }
        }
    }

    private boolean setPassiveIfIsNotShowing(SubCategoryItem subCategoryItemAll) {
        ArrayList<SubCategoryItem> subCategoryItemListShowed = mTableSubCategory.getData();
        boolean setPassiveIfIsNotShowing = true;

        for (SubCategoryItem subCategoryItemShowed : subCategoryItemListShowed) {
            if (subCategoryItemAll.getId().equals(subCategoryItemShowed.getId())) {
                setPassiveIfIsNotShowing = false;
                break;
            }
        }

        return setPassiveIfIsNotShowing;
    }

    public int getActiveSize() {
        return mTableCategory.getActiveSize();
    }

    public boolean isSelectable() {
        if (activeSize < MAX_CATEGORY_TO_SELECT) {
            activeSize = activeSize + 1;
            return true;
        } else {
            showToast(R.string.max_category, MAX_CATEGORY_TO_SELECT);
            return false;
        }
    }

    public void showToast(int message, int number) {
        String toastMessage = getString(message, String.valueOf(number));
        Toast toast = Toast.makeText(getActivity(), toastMessage, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.BOTTOM, 0, 180);
        View toastView = toast.getView();
        toastView.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN);
        TextView text = toastView.findViewById(android.R.id.message);
        text.setTextColor(Color.WHITE);
        text.setTextSize(13);
        toast.show();
    }

    public ArrayList<CategoryItem> getCategoryItemList() {
        return mCategoryItemList;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mCategoryListener = (CategoryListener) getParentFragment();
    }

    public interface CategoryListener {
        void onCategorySelected();
    }
}
