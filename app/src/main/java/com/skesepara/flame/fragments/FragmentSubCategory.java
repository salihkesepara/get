package com.skesepara.flame.fragments;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.skesepara.flame.R;
import com.skesepara.flame.db.DbAdapter;
import com.skesepara.flame.db.TableSubCategory;
import com.skesepara.flame.dialogs.SourceDialog;
import com.skesepara.flame.listview.SubCategoryAdapter;
import com.skesepara.flame.listview.SubCategoryItem;

import java.util.ArrayList;
import java.util.Objects;

public class FragmentSubCategory extends Fragment implements SourceDialog.SourceDialogCategoryListener {
    private static final String TAG = FragmentSubCategory.class.getSimpleName();
    public final static String ACTIVE = "1";
    public final static String PASSIVE = "0";
    public final static int EMPTY_IMAGE = 0;
    private final static int MAX_SUB_CATEGORY_TO_SELECT = 5;
    public final static int MIN_SUB_CATEGORY_TO_SELECT = 1;
    private TableSubCategory tableSubCategory;

    /* objects */
    private View mView;
    private ArrayList<SubCategoryItem> mSubCategoryItemList;

    /* variables */
    int activeSize = -1;

    public FragmentSubCategory() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_subcategory, container, false);

        initListSubCategory(mView);
        return mView;
    }

    public void initListSubCategory(View view) {
        ListView listViewSubCategory = view.findViewById(R.id.listViewSubCategoryDialog);

        /* We need dbAdapter for getting the tableSubCategory methods */
        DbAdapter dbAdapter = new DbAdapter(getContext());

        /* Define a tableSubCategory object for call the getData() method */
        tableSubCategory = new TableSubCategory(getContext(), dbAdapter);

        /* Get subCategory's all data */
        mSubCategoryItemList = tableSubCategory.getData();

        /* Define subCategoryAdapter for listView */
        SubCategoryAdapter subCategoryAdapter = new SubCategoryAdapter(getContext(), mSubCategoryItemList);

        /* setAdapter into listView */
        listViewSubCategory.setAdapter(subCategoryAdapter);

        activeSize = getActiveSize();

        listViewSubCategory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ImageView img = view.findViewById(R.id.imageViewCheck);
                if (Objects.equals(mSubCategoryItemList.get(position).getActive(), ACTIVE)) {
                    tableSubCategory.updateActive(mSubCategoryItemList.get(position).getId(), PASSIVE);
                    mSubCategoryItemList.get(position).setActive(PASSIVE);
                    img.setImageResource(EMPTY_IMAGE);
                    activeSize = activeSize - 1;
                } else {
                    if (isSelectable()) {
                        tableSubCategory.updateActive(mSubCategoryItemList.get(position).getId(), ACTIVE);
                        mSubCategoryItemList.get(position).setActive(ACTIVE);
                        img.setImageResource(R.drawable.ic_check_list);
                    }
                }
            }
        });
    }

    public int getActiveSize() {
        return tableSubCategory.getActiveSize();
    }

    public boolean isSelectable() {
        if (activeSize < MAX_SUB_CATEGORY_TO_SELECT) {
            activeSize = activeSize + 1;
            return true;
        } else {
            showToast(R.string.max_sub_category, MAX_SUB_CATEGORY_TO_SELECT );
            return false;
        }
    }

    public void showToast(int message, int number) {
        String toastMessage = getString(message, String.valueOf(number));
        Toast toast = Toast.makeText(getActivity(), toastMessage, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.BOTTOM, 0, 180);
        View toastView = toast.getView();
        toastView.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN);
        TextView text = toastView.findViewById(android.R.id.message);
        text.setTextColor(Color.WHITE);
        text.setTextSize(13);
        toast.show();
    }

    public ArrayList<SubCategoryItem> getSubCategoryItemList() {
        return mSubCategoryItemList;
    }

    @Override
    public void categorySelected() {
        initListSubCategory(mView);
        Log.v(TAG, "categorySelected");
    }
}
