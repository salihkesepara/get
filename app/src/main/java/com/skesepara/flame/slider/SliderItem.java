package com.skesepara.flame.slider;

public class SliderItem {
    private int mId;

    private String mTitle;

    private String mImage;

    private String mLink;

    public SliderItem(int id, String title, String image, String link) {
        mId = id;
        mTitle = title;
        mImage = image;
        mLink = link;
    }

    public int getId() {
        return mId;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getImage() {
        return mImage;
    }

    public String getLink() {
        return mLink;
    }
}
