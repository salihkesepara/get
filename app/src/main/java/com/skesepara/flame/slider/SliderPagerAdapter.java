package com.skesepara.flame.slider;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.skesepara.flame.R;
import com.skesepara.flame.WebViewPage;
import com.skesepara.flame.db.TableSettings;
import com.skesepara.flame.recyclerview.NewsAdapter;
import com.skesepara.flame.utilities.NetworkUtils;
import com.squareup.picasso.Picasso;

import java.util.List;

public class SliderPagerAdapter extends PagerAdapter {
    private static final String TAG = SliderPagerAdapter.class.getSimpleName();

    private Context mContext;
    private List<SliderItem> mSliderItemList;
    private TableSettings mTableSettings;

    public SliderPagerAdapter(Context context, List<SliderItem> sliderItemList,
                              TableSettings tableSettings) {
        mContext = context;
        mSliderItemList = sliderItemList;
        mTableSettings = tableSettings;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        final SliderItem selectedSliderItem = mSliderItemList.get(position);

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View slideLayout = inflater.inflate(R.layout.item_slider, null);

        RelativeLayout relativeLayoutItemSlider = slideLayout.findViewById(R.id.relativeLayoutItemSlider);
        ImageView imageViewSlider = slideLayout.findViewById(R.id.imageViewSlider);
        TextView textViewSliderTitle = slideLayout.findViewById(R.id.textViewSliderTitle);

        relativeLayoutItemSlider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!NetworkUtils.isNetworkConnection(mContext)) return;
                String link = selectedSliderItem.getLink();

                Intent intent = new Intent(mContext, WebViewPage.class);
                intent.putExtra(WebViewPage.LINK, link);
                mContext.startActivity(intent);
                ++NewsAdapter.WEBVIEW_COUNTER;
            }
        });

        boolean isImageDownloadable = mTableSettings.getIsImageDownloadable().equals(TableSettings.IMAGE_DOWNLOADABLE);
        if (isImageDownloadable) {
            Picasso.get()
                    .load(selectedSliderItem.getImage())
                    .into(imageViewSlider);
        }

        textViewSliderTitle.setText(selectedSliderItem.getTitle());

        container.addView(slideLayout);
        return slideLayout;
    }

    @Override
    public int getCount() {
        return mSliderItemList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view == o;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }
}
