package com.skesepara.flame;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;

import com.skesepara.flame.dialogs.SourceDialog;
import com.skesepara.flame.listview.CategoryItem;
import com.skesepara.flame.listview.SubCategoryItem;
import com.skesepara.flame.utilities.NetworkChangeReceiver;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, SourceDialog.SourceDialogListener {
    private static final String TAG = MainActivity.class.getSimpleName();
    private DrawerLayout drawer;
    private Context context = this;
    public static SearchView searchView;

    /* fragments */
    DashboardFragment dashboardFragment;

    /* objects */
    NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        scheduleJob(); // Check network status
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawerLayout);
        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.open, R.string.close);

        drawer.addDrawerListener(toggle);
        toggle.syncState();

        dashboardFragment = new DashboardFragment();

        getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer,
                dashboardFragment).commit();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void scheduleJob() {
        JobInfo myJob = new JobInfo.Builder(0, new ComponentName(this, NetworkChangeReceiver.class))
                .setRequiresCharging(true)
                .setMinimumLatency(1000)
                .setOverrideDeadline(2000)
                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                .setPersisted(true)
                .build();

        JobScheduler jobScheduler = (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);
        jobScheduler.schedule(myJob);
    }

    @Override
    protected void onStop() {
        // A service can be "started" and/or "bound". In this case, it's "started" by this Activity
        // and "bound" to the JobScheduler (also called "Scheduled" by the JobScheduler). This call
        // to stopService() won't prevent scheduled jobs to be processed. However, failing
        // to call stopService() would keep it alive indefinitely.
        stopService(new Intent(context, NetworkChangeReceiver.class));
        super.onStop();
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Start service and provide it a way to communicate with this class.
        Intent startServiceIntent = new Intent(this, NetworkChangeReceiver.class);
        startService(startServiceIntent);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            new AlertDialog.Builder(this)
                .setMessage(getString(R.string.exit_message))
                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        MainActivity.super.onBackPressed();
                    }
                })
                .setNegativeButton(getString(R.string.no), null)
                .show();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        Menu menu = navigationView.getMenu();
        String packageName = getPackageName();
        String linkURL = "http://play.google.com/store/apps/details?id=" + packageName;
        String marketURL = "market://details?id=" + packageName;

        switch (menuItem.getItemId()) {
            case R.id.dashboard:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer,
                        dashboardFragment).commit();
                drawer.closeDrawer(GravityCompat.START);
                menu.findItem(R.id.source).setVisible(true);
                break;
            case R.id.source:
                showSourceDialog();
                break;
            case R.id.settings:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer,
                        new SettingsFragment()).commit();
                drawer.closeDrawer(GravityCompat.START);
                menu.findItem(R.id.source).setVisible(false);
                break;
            case R.id.rateApp:
                try {
                    Uri uri = Uri.parse(marketURL);
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    Log.e(TAG, "ActivityNotFoundException: " + e.getMessage());
                    Uri uri = Uri.parse(linkURL);
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);
                }
                drawer.closeDrawer(GravityCompat.START);
                break;
            case R.id.share:
                try {
                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setType("text/plain");
                    shareIntent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name_full));
                    String shareMessage= getResources().getString(R.string.share_app_description) +"\n\n";
                    shareMessage = shareMessage + linkURL +"\n\n";
                    shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                    startActivity(Intent.createChooser(shareIntent, getResources().getString(R.string.share_app_title)));
                } catch (ActivityNotFoundException e) {
                    Log.e(TAG, "ActivityNotFoundException: " + e.getMessage());
                }
                drawer.closeDrawer(GravityCompat.START);
                break;
//            case R.id.noAds:
//                getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer,
//                        new NoAdsFragment()).commit();
//                break;
        }

        return true;
    }

    public void showSourceDialog() {
        SourceDialog sourceDialog = new SourceDialog();

        Fragment fragment = getSupportFragmentManager().findFragmentByTag("SourceDialog");

        if (fragment == null) {
            sourceDialog.show(getSupportFragmentManager(), "SourceDialog");
        }
    }

    @Override
    public void sourceDialogOnDone(ArrayList<CategoryItem> categoryItemList, ArrayList<SubCategoryItem> subCategoryItemList) {
        dashboardFragment.sourceDialogOnDone(categoryItemList, subCategoryItemList);
        drawer.closeDrawer(GravityCompat.START);
    }

    @Override
    public void sourceDialogOnCancel() {
        dashboardFragment.sourceDialogOnCancel();
        drawer.closeDrawer(GravityCompat.START);
    }

    @Override
    protected void onDestroy() {
        dashboardFragment.clearOldNews();
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);

        MenuItem menuItem = menu.findItem(R.id.menuSearch);
        searchView = (SearchView) menuItem.getActionView();
        searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                dashboardFragment.filterAdapter(newText);
                return false;
            }
        });

        searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b) {
                    if (searchView.getQuery().toString().trim().equals("")) {
                        dashboardFragment.clearSearchView();
                    }
                }
            }
        });

        return true;
    }


}
