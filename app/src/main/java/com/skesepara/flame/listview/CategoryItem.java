package com.skesepara.flame.listview;

public class CategoryItem {
    private String mId;
    private String mName;
    private String mImg;
    private String mPattern;
    private String mActive;

    public CategoryItem(String id, String name, String img, String pattern, String active) {
        mId = id;
        mName = name;
        mImg = img;
        mActive = active;
        mPattern = pattern;
    }

    public String getId() {
        return mId;
    }

    public String getName() {
        return mName;
    }

    public String getImg() {
        return mImg;
    }

    public String getActive() {
        return mActive;
    }

    public String getPattern() {
        return mPattern;
    }

    public void setActive(String active) {
        mActive = active;
    }
}
