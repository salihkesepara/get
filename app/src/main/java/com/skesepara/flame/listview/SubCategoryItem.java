package com.skesepara.flame.listview;

public class SubCategoryItem {
    private String mId;
    private String mName;
    private String mImg;
    private String mActive;

    public SubCategoryItem(String id, String name, String img, String active) {
        mId = id;
        mName = name;
        mImg = img;
        mActive = active;
    }

    public String getId() {
        return mId;
    }

    public String getName() {
        return mName;
    }

    public String getImg() {
        return mImg;
    }

    public String getActive() {
        return mActive;
    }

    public void setActive(String active) {
        mActive = active;
    }
}
