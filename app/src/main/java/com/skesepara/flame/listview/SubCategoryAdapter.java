package com.skesepara.flame.listview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.skesepara.flame.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class SubCategoryAdapter extends BaseAdapter {
    private static final String TAG = SubCategoryAdapter.class.getSimpleName();
    List<SubCategoryItem> mSubCategoryItem = new ArrayList<>();
    Context mContext;

    public SubCategoryAdapter(Context context, List<SubCategoryItem> subCategoryItem) {
        mContext = context;
        mSubCategoryItem = subCategoryItem;
    }

    /* Listview de gösterilecek satır sayısı */
    @Override
    public int getCount() {
        return mSubCategoryItem.size();
    }

    /* Position ile sırası gelen eleman */
    @Override
    public Object getItem(int position) {
        return null;
    }

    /* Varsa niteleyici id bilgisi */
    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemSubCategory = LayoutInflater.from(mContext).inflate(R.layout.item_subcategory, null);
        TextView textViewName = itemSubCategory.findViewById(R.id.textViewName);
        ImageView imageView = itemSubCategory.findViewById(R.id.imageViewLogo);
        ImageView imageViewCheck = itemSubCategory.findViewById(R.id.imageViewCheck);

        SubCategoryItem subCategoryItem = mSubCategoryItem.get(position);
        textViewName.setText(subCategoryItem.getName());
        imageView.setImageResource(Integer.parseInt(subCategoryItem.getImg()));

        if (Objects.equals(subCategoryItem.getActive(), "1")) {
            imageViewCheck.setImageResource(R.drawable.ic_check_list);
        }

        return itemSubCategory;
    }
}
