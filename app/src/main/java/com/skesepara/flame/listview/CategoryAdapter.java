package com.skesepara.flame.listview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.skesepara.flame.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class CategoryAdapter extends BaseAdapter {
    private static final String TAG = CategoryAdapter.class.getSimpleName();
    List<CategoryItem> mCategoryItem = new ArrayList<>();
    Context mContext;

    public CategoryAdapter(Context context, List<CategoryItem> categoryItem) {
        mContext = context;
        mCategoryItem = categoryItem;
    }

    /* Listview de gösterilecek satır sayısı */
    @Override
    public int getCount() {
        return mCategoryItem.size();
    }

    /* Position ile sırası gelen eleman */
    @Override
    public Object getItem(int position) {
        return null;
    }

    /* Varsa niteleyici id bilgisi */
    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemCategory = LayoutInflater.from(mContext).inflate(R.layout.item_category, null);
        TextView textViewName = itemCategory.findViewById(R.id.textViewName);
        ImageView imageView = itemCategory.findViewById(R.id.imageViewLogo);
        ImageView imageViewCheck = itemCategory.findViewById(R.id.imageViewCheck);

        CategoryItem categoryItem = mCategoryItem.get(position);
        textViewName.setText(categoryItem.getName());
        imageView.setImageResource(Integer.parseInt(categoryItem.getImg()));

        if (Objects.equals(categoryItem.getActive(), "1")) {
            imageViewCheck.setImageResource(R.drawable.ic_check_list);
        }

        return itemCategory;
    }
}
