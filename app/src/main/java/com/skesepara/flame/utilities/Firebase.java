package com.skesepara.flame.utilities;

import android.support.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Objects;

public class Firebase {
    private static final String TAG = Firebase.class.getSimpleName();
    private final String ADS = "ADS";
    public static final String BANNER = "BANNER";
    public static final String INTERSTITIAL = "INTERSTITIAL";
    public static final String ACTIVATED_ADS = "ACTIVATED_ADS";
    public static final String ITEM_PER_AD = "ITEM_PER_AD";

    /* Interfaces */
    private FirebaseListener mFirebaseListener;

    /* Objects */
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mDbRef;

    public Firebase(FirebaseListener firebaseListener) {
        this.mFirebaseListener = firebaseListener;
        this.mFirebaseDatabase = FirebaseDatabase.getInstance();
        this.mDbRef = this.mFirebaseDatabase.getReference(ADS);

        initListiner();
    }

    public static Firebase build(@NonNull FirebaseListener firebaseListener) {
        return new Firebase(firebaseListener);
    }

    private void initListiner() {
         final FirebaseListener firebaseListener = this.mFirebaseListener;
        this.mDbRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                firebaseListener.adsOnChanged(dataSnapshot);
                Objects ads = (Objects) dataSnapshot.getValue();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public interface FirebaseListener {
        void adsOnChanged(DataSnapshot dataSnapshot);
    }
}
