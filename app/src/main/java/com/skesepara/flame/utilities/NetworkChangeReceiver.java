package com.skesepara.flame.utilities;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.util.Log;
import android.widget.Toast;

import com.skesepara.flame.R;

public class NetworkChangeReceiver extends JobService implements
        ConnectivityReceiver.ConnectivityReceiverListener {
    private static final String TAG = NetworkChangeReceiver.class.getSimpleName();

    private ConnectivityReceiver mConnectivityReceiver;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(TAG, "Service created");
        mConnectivityReceiver = new ConnectivityReceiver(this);
    }

    /**
     * When the app's NetworkConnectionActivity is created, it starts this service. This is so that the
     * activity and this service can communicate back and forth. See "setUiCallback()"
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "onStartCommand");
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public boolean onStartJob(JobParameters params) {
        Log.i(TAG, "onStartJob" + mConnectivityReceiver);
        registerReceiver(mConnectivityReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters jobParameters) {
        Log.i(TAG, "onStopJob");
        unregisterReceiver(mConnectivityReceiver);
        return false;
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        NetworkUtils.setNetworkConnection(isConnected);
    }

    @Override
    public void onWifiConnectionChanged(boolean isWifi) {
        NetworkUtils.setWIFI(isWifi);
    }

    @Override
    public void onMobileConnectionChanged(boolean isMobile) {
        NetworkUtils.setMOBILE(isMobile);
    }
}
