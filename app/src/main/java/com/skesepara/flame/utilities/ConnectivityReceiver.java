package com.skesepara.flame.utilities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class ConnectivityReceiver extends BroadcastReceiver {
    private ConnectivityReceiverListener mConnectivityReceiverListener;
    private static final int CONNECTED = 1;
    private static final int WIFI = 2;
    private static final int MOBILE = 3;

    public ConnectivityReceiver(ConnectivityReceiverListener connectivityReceiverListener) {
        this.mConnectivityReceiverListener = connectivityReceiverListener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        mConnectivityReceiverListener.onNetworkConnectionChanged(checkNetwork(context, CONNECTED));
        mConnectivityReceiverListener.onWifiConnectionChanged(checkNetwork(context, WIFI));
        mConnectivityReceiverListener.onMobileConnectionChanged(checkNetwork(context, MOBILE));
    }

    public static boolean checkNetwork(Context context, int status) {
        boolean type = false;
        ConnectivityManager connectivityManager = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();

        boolean isConnected = activeNetwork != null && activeNetwork.isConnected();

        switch (status) {
            case CONNECTED:
                type = isConnected;
                break;
            case WIFI:
                type = isConnected && activeNetwork.getType() == ConnectivityManager.TYPE_WIFI;
                break;
            case MOBILE:
                type = isConnected && activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE;
                break;
        }

        return type;
    }

    public interface ConnectivityReceiverListener {
        void onNetworkConnectionChanged(boolean isConnected);
        void onWifiConnectionChanged(boolean isWifi);
        void onMobileConnectionChanged(boolean isMobile);
    }
}
