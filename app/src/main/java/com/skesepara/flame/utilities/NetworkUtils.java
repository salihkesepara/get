package com.skesepara.flame.utilities;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import com.skesepara.flame.R;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * This class create a url and request
 */
public class NetworkUtils {
    private static final String TAG = NetworkUtils.class.getSimpleName();
    private static boolean NETWORK_CONNECTION;
    private static boolean WIFI;
    private static boolean MOBILE;

    /* Format of the request's return type */
    private static final String mFormat = "json";
    private static final String mFORMAT_PARAM = "mode";

    /**
     * isNetworkConnection
     * @param context Context
     * @return boolean
     */
    public static boolean isNetworkConnection(Context context) {
        if (!NETWORK_CONNECTION) {
            Toast.makeText(context, context.getResources().getString(R.string.network_problem),
                    Toast.LENGTH_SHORT).show();
        }

        return NETWORK_CONNECTION;
    }

    public static boolean isNetworkConnection() {
        return NETWORK_CONNECTION;
    }

    public static void setNetworkConnection(boolean networkConnection) {
        NetworkUtils.NETWORK_CONNECTION = networkConnection;
    }

    public static boolean isWifi() {
        return WIFI;
    }

    public static void setWIFI(boolean WIFI) {
        NetworkUtils.WIFI = WIFI;
    }

    public static boolean isMobile() {
        return MOBILE;
    }

    public static void setMOBILE(boolean MOBILE) {
        NetworkUtils.MOBILE = MOBILE;
    }

    /**
     * Build the url
     * @return URL
     */
    public static URL buildUrl(String feedURL) {
        Uri buildUri = Uri.parse(feedURL).buildUpon()
                .appendQueryParameter(mFORMAT_PARAM, mFormat)
                .build();
        URL url = null;

        try {
            url = new URL(buildUri.toString());
        } catch (Exception e) {
            Log.e(TAG, "buildUrl: Exception: " + e.getMessage());
        }

        return url;
    }

    /**
     * Returns the result of the request made with the web service.
     * @param url source
     * @return String response
     * @throws IOException e
     */
    public static Document getResponseFromHttpUrl(URL url) throws IOException {
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

        try {
            InputStream inputStream = urlConnection.getInputStream();
            DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = builderFactory.newDocumentBuilder();
            Document xmlDoc = builder.parse(inputStream);
            return xmlDoc;
        } catch (ParserConfigurationException e) {
            Log.e(TAG, "getResponseFromHttpUrl: ParserConfigurationException" + e.getMessage());
            return null;
        } catch (SAXException e) {
            Log.e(TAG, "getResponseFromHttpUrl: SAXException: " + e.getMessage());
            return null;
        } finally {
            urlConnection.disconnect();
        }
    }
}
