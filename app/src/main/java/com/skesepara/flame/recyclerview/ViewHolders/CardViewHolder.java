package com.skesepara.flame.recyclerview.ViewHolders;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.skesepara.flame.R;
import com.skesepara.flame.WebViewPage;
import com.skesepara.flame.db.TableSettings;
import com.skesepara.flame.recyclerview.NewsAdapter;
import com.skesepara.flame.recyclerview.NewsItem;
import com.skesepara.flame.utilities.NetworkUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class CardViewHolder extends RecyclerView.ViewHolder {
    private static final String TAG = CardViewHolder.class.getSimpleName();
    private ImageView ivItemPhoto, imageViewLogoCardNews;
    private TextView tvItemTitle, tvDescription, tvCategory, tvSubCategory, textViewPubDate;
    private LinearLayout linearLayoutNews;
    private Button buttonShare;
    private ArrayList<NewsItem> mNewsItemList;
    private Context mContext;
    private TableSettings mTableSettings;

    public CardViewHolder(View itemView, ArrayList<NewsItem> newsItemList, Context context,
                          TableSettings tableSettings) {
        super(itemView);
        ivItemPhoto = itemView.findViewById(R.id.ivItemPhoto);
        tvItemTitle = itemView.findViewById(R.id.tvItemTitle);
        tvDescription = itemView.findViewById(R.id.tvDescription);
        tvCategory = itemView.findViewById(R.id.tvCategory);
        tvSubCategory = itemView.findViewById(R.id.tvSubCategory);
        linearLayoutNews = itemView.findViewById(R.id.linearLayoutNews);
        imageViewLogoCardNews = itemView.findViewById(R.id.imageViewLogoCardNews);
        textViewPubDate = itemView.findViewById(R.id.textViewPubDate);
        buttonShare = itemView.findViewById(R.id.buttonShare);
        mNewsItemList = newsItemList;
        mContext = context;
        mTableSettings = tableSettings;


    }

    public void setData(NewsItem selectedNewsItem, final int position) {
        this.tvItemTitle.setText(selectedNewsItem.getTitle());
        this.tvDescription.setText(selectedNewsItem.getDescription());
        this.tvCategory.setText(selectedNewsItem.getCategory());
        this.tvSubCategory.setText(selectedNewsItem.getSubCategory());
        this.textViewPubDate.setText(selectedNewsItem.getPubDate().substring(0, 10));

        boolean isImageDownloadable = mTableSettings.getIsImageDownloadable().equals(TableSettings.IMAGE_DOWNLOADABLE);
        if (isImageDownloadable) {
            Picasso.get()
                    .load(selectedNewsItem.getImage())
                    .into(this.ivItemPhoto);
        }

        Picasso.get()
                .load(Integer.parseInt(selectedNewsItem.getCategoryImg()))
                .into(this.imageViewLogoCardNews);

        linearLayoutNews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!NetworkUtils.isNetworkConnection(mContext)) return;

                String link = mNewsItemList.get(position).getLink();

                Intent intent = new Intent(mContext, WebViewPage.class);
                intent.putExtra(WebViewPage.LINK, link);
                mContext.startActivity(intent);
                ++NewsAdapter.WEBVIEW_COUNTER;
            }
        });

        buttonShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!NetworkUtils.isNetworkConnection(mContext)) return;

                NewsItem selectedNewsItem  = mNewsItemList.get(position);
                String packageName = mContext.getPackageName();
                String linkURL = "http://play.google.com/store/apps/details?id=" + packageName;
                String text = selectedNewsItem.getTitle() + "\n\n" +
                        selectedNewsItem.getDescription() + "\n\n" +
                        linkURL;

                try {
                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setType("text/plain");
                    shareIntent.putExtra(Intent.EXTRA_TEXT, text);
                    shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    mContext.startActivity(Intent.createChooser(shareIntent, mContext.getResources().getString(R.string.share_app)));
                } catch (ActivityNotFoundException e) {
                    Log.e(TAG, "ActivityNotFoundException: " + e.getMessage());
                }
            }
        });
    }
}
