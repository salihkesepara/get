package com.skesepara.flame.recyclerview.ViewHolders;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.skesepara.flame.R;
import com.skesepara.flame.db.TableSettings;
import com.skesepara.flame.slider.SliderItem;
import com.skesepara.flame.slider.SliderPagerAdapter;

import java.util.List;

public class SliderViewHolder extends RecyclerView.ViewHolder {
    private static final String TAG = SliderViewHolder.class.getSimpleName();
    private ViewPager mSliderPage;
    private Context mContext;
    private LinearLayout mDotLayout;
    private List<SliderItem> mSliderItemList;
    private TableSettings mTableSettings;
    private ImageView imageViewArrowLeft;
    private ImageView imageViewArrowRight;

    public SliderViewHolder(@NonNull View itemView, Context context, List<SliderItem> sliderItemList,
                            TableSettings tableSettings) {
        super(itemView);
        mSliderPage = itemView.findViewById(R.id.sliderPager);
        mDotLayout = itemView.findViewById(R.id.linearLayoutIndicator);
        imageViewArrowLeft = itemView.findViewById(R.id.imageViewArrowLeft);
        imageViewArrowRight = itemView.findViewById(R.id.imageViewArrowRight);
        mContext = context;
        mSliderItemList = sliderItemList;
        mTableSettings = tableSettings;
    }

    public void setData(List<SliderItem> sliderItemList) {
        SliderPagerAdapter adapter = new SliderPagerAdapter(mContext, sliderItemList, mTableSettings);
        mSliderPage.setAdapter(adapter);

        addDotsIndicator(0);
        mSliderPage.addOnPageChangeListener(viewPageListener);

        imageViewArrowLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int currentPage = mSliderPage.getCurrentItem();
                int lastSliderPage = mSliderItemList.size() - 1;

                if (currentPage == 0) {
                    mSliderPage.setCurrentItem(lastSliderPage, true);
                } else {
                    mSliderPage.setCurrentItem(currentPage - 1, true);
                }
            }
        });

        imageViewArrowRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int currentPage = mSliderPage.getCurrentItem();
                int sliderSize = mSliderItemList.size() - 1;

                if (currentPage == sliderSize) {
                    mSliderPage.setCurrentItem(0, true);
                } else {
                    mSliderPage.setCurrentItem(currentPage + 1, true);
                }
            }
        });
    }

    public void addDotsIndicator(int position) {
        TextView[] mDots = new TextView[mSliderItemList.size()];
        mDotLayout.removeAllViews();

        for (int i = 0; i < mDots.length; i++) {
            mDots[i] = new TextView(mContext);
            mDots[i].setText(Html.fromHtml("&#8226"));
            mDots[i].setTextColor(mContext.getResources().getColor(R.color.colorFont));
            mDots[i].setTextSize(27);
            mDotLayout.addView(mDots[i]);
        }

        mDots[position].setTextColor(mContext.getResources().getColor(R.color.colorPrimary));
    }

    ViewPager.OnPageChangeListener viewPageListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int i, float v, int i1) {

        }

        @Override
        public void onPageSelected(int position) {
            addDotsIndicator(position);
        }

        @Override
        public void onPageScrollStateChanged(int i) {

        }
    };
}
