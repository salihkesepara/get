package com.skesepara.flame.recyclerview.ViewHolders;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.skesepara.flame.R;

public class AdsViewHolder extends RecyclerView.ViewHolder {
    private static final String TAG = AdsViewHolder.class.getSimpleName();
    private AdView adView;
    private Context mContext;

    public AdsViewHolder(@NonNull View itemView, Context mContext) {
        super(itemView);
        adView = itemView.findViewById(R.id.adView);
        String adMobAppId = mContext.getResources().getString(R.string.ad_mob_app_id);
        MobileAds.initialize(mContext, adMobAppId);
    }

    public void setData(AdRequest adRequest) {
        this.adView.loadAd(adRequest);

        this.adView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
                System.out.println("onAdLoaded");
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
                Log.e("onAdFailedToLoad", Integer.toString(errorCode));
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
                System.out.println("onAdOpened");
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
                System.out.println("onAdLeftApplication");
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when when the user is about to return
                // to the app after tapping on an ad.
                System.out.println("onAdClosed");
            }
        });
    }
}
