package com.skesepara.flame.recyclerview;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.google.android.gms.ads.AdRequest;
import com.skesepara.flame.R;
import com.skesepara.flame.db.TableSettings;
import com.skesepara.flame.recyclerview.ViewHolders.AdsViewHolder;
import com.skesepara.flame.recyclerview.ViewHolders.CardViewHolder;
import com.skesepara.flame.recyclerview.ViewHolders.SliderViewHolder;
import com.skesepara.flame.slider.SliderItem;
import com.skesepara.flame.utilities.NetworkUtils;

import java.util.ArrayList;
import java.util.List;

public class NewsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {
    private static final String TAG = NewsAdapter.class.getSimpleName();

    private static final int SLIDER_VIEW = 0;
    private static final int BANNER_AD_VIEW_TYPE = 1;
    private static final int NEWS_ITEM_VIEW_TYPE = 2;
    public static boolean ACTIVATED_ADS = true;
    public static boolean SHOW_SLIDER = true;
    public static int ITEMS_PER_AD  = 6;
    public static int WEBVIEW_COUNTER = 0;

    private ArrayList<NewsItem> mNewsItemList;
    private ArrayList<NewsItem> mNewsItemListCache;
    private ArrayList<NewsItem> mNewsItemListFull;
    private List<SliderItem> mSliderItemList;
    private LayoutInflater mInflater;
    private Context mContext;
    private TableSettings mTableSettings;

    public NewsAdapter(Context context, ArrayList<NewsItem> newsItemList, ArrayList<NewsItem> newsItemListFull,
                       List<SliderItem> sliderItemList, TableSettings tableSettings) {
        mInflater = LayoutInflater.from(context);
        mNewsItemList = newsItemList;
        mNewsItemListCache = new ArrayList<>(mNewsItemList);
        mNewsItemListFull = newsItemListFull;
        mSliderItemList = sliderItemList;
        mContext = context;
        mTableSettings = tableSettings;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return SHOW_SLIDER ? SLIDER_VIEW : NEWS_ITEM_VIEW_TYPE;
        } else {
            if (ACTIVATED_ADS &&
                    position % ITEMS_PER_AD == 0 &&
                    NetworkUtils.isNetworkConnection()) {
                return BANNER_AD_VIEW_TYPE;
            } else {
                return NEWS_ITEM_VIEW_TYPE;
            }
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        if (viewType == SLIDER_VIEW) {
            View viewSlider = mInflater.inflate(R.layout.slider_pager, viewGroup, false);
            return new SliderViewHolder(viewSlider, mContext, mSliderItemList, mTableSettings);
        } else if (viewType == NEWS_ITEM_VIEW_TYPE) {
            View viewCard = mInflater.inflate(R.layout.item_news_card, viewGroup, false);
            return new CardViewHolder(viewCard, mNewsItemList, mContext, mTableSettings);
        } else {
            // BANNER_AD_VIEW_TYPE
            View viewAds = mInflater.inflate(R.layout.add_mob, viewGroup, false);
            return new AdsViewHolder(viewAds, mContext);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        int viewType = holder.getItemViewType();

        if (viewType == SLIDER_VIEW) {
            SliderViewHolder sliderViewHolder= (SliderViewHolder) holder;
            sliderViewHolder.setData(mSliderItemList);
        } else if (viewType == NEWS_ITEM_VIEW_TYPE) {
            NewsItem selectedNewsItem = mNewsItemList.get(position);
            CardViewHolder cardViewHolder = (CardViewHolder) holder;
            cardViewHolder.setData(selectedNewsItem, position);
        } else {
            // BANNER_AD_VIEW_TYPE
            AdsViewHolder adsViewHolder = (AdsViewHolder) holder;
            AdRequest adRequest = new AdRequest.Builder()
//                    .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                    .build();
            adsViewHolder.setData(adRequest);
        }
    }

    @Override
    public int getItemCount() {
        if (mNewsItemList.size() != 0) {
            return mNewsItemList.size();
        } else {
            return SHOW_SLIDER ? 1 : 0;
        }

    }

    @Override
    public Filter getFilter() {
        return newsFilter;
    }

    private Filter newsFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            List<NewsItem> filteredList = new ArrayList<>();

            if (charSequence == null || charSequence.length() == 0) {
                SHOW_SLIDER = true;
                filteredList.addAll(mNewsItemListCache);
            } else {
                SHOW_SLIDER = false;
                String filterPattern = charSequence.toString().toLowerCase().trim();

                for (NewsItem item : mNewsItemListFull) {
                    if (item.getTitle().toLowerCase().contains(filterPattern) ||
                            item.getDescription().toLowerCase().contains(filterPattern) ||
                            item.getCategory().toLowerCase().contains(filterPattern) ||
                            item.getSubCategory().toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);
                    }
                }
            }

            FilterResults filterResults = new FilterResults();
            filterResults.values = filteredList;

            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            mNewsItemList.clear();
            mNewsItemList.addAll((List) filterResults.values);
            notifyDataSetChanged();
        }
    };
}
