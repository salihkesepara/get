package com.skesepara.flame.recyclerview;

public class NewsItem {
    /* id of news */
    private int mId;

    private String mTitle;

    private String mImage;

    private String mDescription;

    private String mCategory;

    private String mPubDate;

    private String mSubCategory;

    private String mLink;

    private String mCategoryImg;

    public NewsItem(int id, String title, String image, String description, String pubDate, String category, String subCategory, String link, String categoryImg) {
        mId = id;
        mTitle = title;
        mImage = image;
        mDescription = description;
        mPubDate = pubDate;
        mCategory = category;
        mSubCategory = subCategory;
        mLink = link;
        mCategoryImg = categoryImg;
    }

    public int getId() {
        return mId;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getImage() {
        return mImage;
    }

    public String getDescription() {
        return mDescription;
    }

    public String getPubDate() {
        return mPubDate;
    }

    public String getCategory() {
        return mCategory;
    }

    public String getSubCategory() {
        return mSubCategory;
    }

    public String getLink() {
        return mLink;
    }

    public String getCategoryImg() {
        return mCategoryImg;
    }
}
