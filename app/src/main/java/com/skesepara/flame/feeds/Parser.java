package com.skesepara.flame.feeds;

import android.util.Log;

import com.skesepara.flame.db.TableCategory;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Parser {
    private static final String TAG = Parser.class.getSimpleName();
    public static final String OUTPUT_PATTERN = "yyyy-MM-dd HH:mm:ss";

    public static String getDate(String pubDate, String inputPattern) {
        Date date = null;

        SimpleDateFormat formatter = new SimpleDateFormat(inputPattern, Locale.ENGLISH);
        try {
            date = formatter.parse(pubDate);
        } catch (Exception e) {
            Log.e(TAG, "getDate: Exception" + e.getMessage());
        }
        formatter = new SimpleDateFormat(OUTPUT_PATTERN, Locale.ENGLISH);

        return formatter.format(date);
    }

    public static String getImage(Node current, String categoryId) {
        String image = null;

        switch (categoryId) {
            case TableCategory.CNN:
                image = current.getTextContent();
                break;
            case TableCategory.ENSONHABER:
                image = current.getTextContent();
                break;
            case TableCategory.YENISAFAK:
                image = current.getChildNodes().item(1).getTextContent();
                break;
            case TableCategory.HURRIYET:
                image = "http://" + current.getTextContent().substring(7);
                break;
            case TableCategory.CHIP:
                image = current.getTextContent();
                break;
            case TableCategory.STAR:
                image = current.getTextContent();
                break;
            case TableCategory.MYNET:
                image = current.getTextContent();
                break;
            case TableCategory.SABAH:
                image = current.getTextContent();
                break;
            case TableCategory.AHABER:
                // NO IMAGE
                break;
            case TableCategory.MILLIYET:
                image = current.getTextContent();
                break;
        }

        return image;
    }

    public static Node getChannel(Element root, String categoryId) {
        Node channel = null;

        switch (categoryId) {
            case TableCategory.CNN:
                channel = root.getChildNodes().item(1);
                break;
            case TableCategory.YENISAFAK:
                channel = root.getChildNodes().item(1);
                break;
            case TableCategory.HURRIYET:
                channel = root.getChildNodes().item(0);
                break;
            case TableCategory.CHIP:
                channel = root.getChildNodes().item(0);
                break;
            case TableCategory.ENSONHABER:
                channel = root.getChildNodes().item(1);
                break;
            case TableCategory.STAR:
                channel = root.getChildNodes().item(1);
                break;
            case TableCategory.MYNET:
                channel = root.getChildNodes().item(1);
                break;
            case TableCategory.SABAH:
                channel = root.getChildNodes().item(1);
                break;
            case TableCategory.AHABER:
                channel = root.getChildNodes().item(1);
                break;
            case TableCategory.MILLIYET:
                channel = root.getChildNodes().item(1);
                break;
        }

        return channel;
    }

    public static String description(Node current, String categoryId) {
        String desc = null;

        if (categoryId.equals(TableCategory.ENSONHABER)) {
            String text = current.getTextContent();
            for (int i = 0; i < text.length(); i ++) {
                String searchedChar = text.substring(i, i + 3);
                if (searchedChar.equals("</a")) {
                    desc = text.substring(i + 4);
                    break;
                }
            }
        } else if (categoryId.equals(TableCategory.SABAH)) {
            int startPoint = 0;
            int endPoint = 0;
            String startCapture = "<b";
            String endCapture = "<a";
            String text = current.getTextContent();

            for (int i = 0; i < text.length(); i ++) {
                String searchedChar = text.substring(i, i + 2);

                if (searchedChar.equals(startCapture)) {
                    startPoint = i + 6;
                } else if (searchedChar.equals(endCapture)) {
                    endPoint = i;
                    desc = text.substring(startPoint, endPoint);
                    break;
                }
            }
        } else if (categoryId.equals(TableCategory.AHABER)) {
            int startPoint = 0;
            int endPoint = 0;
            String endCapture = "<a";
            String text = current.getTextContent();

            for (int i = 0; i < text.length(); i ++) {
                String searchedChar = text.substring(i, i + 2);

                if (searchedChar.equals(endCapture)) {
                    endPoint = i;

                    desc = text.substring(startPoint, endPoint);
                    break;
                }
            }
        } else if (categoryId.equals(TableCategory.MILLIYET)) {

        }
        else {
            desc = current.getTextContent();
        }

        return desc;
    }
}
