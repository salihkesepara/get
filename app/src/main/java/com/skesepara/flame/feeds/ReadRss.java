package com.skesepara.flame.feeds;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.skesepara.flame.DashboardFragment;
import com.skesepara.flame.db.DbAdapter;
import com.skesepara.flame.db.TableLink;
import com.skesepara.flame.db.TableNews;
import com.skesepara.flame.db.RssLinkItem;
import com.skesepara.flame.db.TableSettings;
import com.skesepara.flame.recyclerview.NewsAdapter;
import com.skesepara.flame.recyclerview.NewsItem;
import com.skesepara.flame.slider.SliderItem;
import com.skesepara.flame.utilities.NetworkUtils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class ReadRss {
    private static final String TAG = ReadRss.class.getSimpleName();
    private Context mContext;
    private TableNews mTableNews;
    private TableSettings mTableSettings;
    private TableLink mTableLink;
    private RecyclerView mRecyclerView;
    private ArrayList<RssLinkItem> mRssLinkItemList;
    private Integer mCounter = 0;
    private SwipeRefreshLayout mSwipeRefreshLayoutNews;
    private LinearLayout mLinearLayoutNoResult;

    public ReadRss(Context context, RecyclerView recyclerView, SwipeRefreshLayout swipeRefreshLayoutNews,
                   LinearLayout linearLayout, ArrayList<RssLinkItem> rssLinkItemList) {
        mContext = context;
        mRecyclerView = recyclerView;
        mSwipeRefreshLayoutNews = swipeRefreshLayoutNews;
        mLinearLayoutNoResult = linearLayout;

        DbAdapter dbAdapter = new DbAdapter(context);
        mTableNews = new TableNews(dbAdapter);
        mTableSettings = new TableSettings(dbAdapter);
        mTableLink = new TableLink(dbAdapter);
        mRssLinkItemList = rssLinkItemList;
    }

    public void fetch() {
        mSwipeRefreshLayoutNews.setRefreshing(true);
        for (RssLinkItem rssLinkItem: mRssLinkItemList) {
            new FetchTask(rssLinkItem.getLink(), rssLinkItem.getCategoryId(),
                    rssLinkItem.getSubCategoryId(), rssLinkItem.getPattern()).execute();
        }
    }

    public class FetchTask extends AsyncTask<String, Void, String[]> {
        private final String mLink;
        private final String mCategoryId;
        private final String mSubCategoryId;
        private final String mPattern;

        public FetchTask(String link, String categoryId, String subCategoryId, String pattern) {
            mLink = link;
            mCategoryId = categoryId;
            mSubCategoryId = subCategoryId;
            mPattern = pattern;
        }

        @Override
        protected String[] doInBackground(String... strings) {
            /* Create URL */
            URL requestURL = NetworkUtils.buildUrl(mLink);

            try {
                /* Get the xmlDoc data */
                Document xmlDoc = NetworkUtils.getResponseFromHttpUrl(requestURL);

                /* Parse xml */
                ProcessXml(xmlDoc);
                return null;
            } catch (Exception e) {
                Log.e(TAG, "doInBackground: Exception: " + e.getMessage());
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(String[] data) {
            mCounter = mCounter + 1;

            /* Load RecyclerView If all request is done */
            if (mCounter == mRssLinkItemList.size()) {
                mSwipeRefreshLayoutNews.setRefreshing(false);

                ArrayList<NewsItem> newsItemList = mTableNews.getData();
                ArrayList<NewsItem> newsItemListFull = mTableNews.getFullData();
                List<SliderItem> sliderItemList = mTableNews.getSliderData();

                if (newsItemList.size() != 0 || sliderItemList.size() != 0) {
                    DashboardFragment.newsAdapter = new NewsAdapter(mContext, newsItemList, newsItemListFull, sliderItemList,
                            mTableSettings);
                    mRecyclerView.setAdapter(DashboardFragment.newsAdapter);

                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
                    linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                    mRecyclerView.setLayoutManager(linearLayoutManager);

                } else {
                    mRecyclerView.setAdapter(null);
                    mLinearLayoutNoResult.setVisibility(ImageView.VISIBLE);
                }

                DashboardFragment.mTotalScrolled = 0;
            }

            mTableLink.updateCreateDate(mCategoryId, mSubCategoryId);

            super.onPostExecute(data);
        }

        /**
         * Parse rss response and update news table
         * @param data Document
         */
        private void ProcessXml(Document data) {
            if (data != null) {
                Element root = data.getDocumentElement();
                Node channel = Parser.getChannel(root, mCategoryId);
                NodeList items = channel.getChildNodes();
                for (int i = 0; i < items.getLength(); i++) {
                    Node currentchild = items.item(i);
                    if (currentchild.getNodeName().equalsIgnoreCase("item")) {
                        NodeList itemchilds = currentchild.getChildNodes();
                        String title = null, description = null, link = null, image = null, pubDate = null;
                        for (int j = 0; j < itemchilds.getLength(); j++) {
                            Node current = itemchilds.item(j);
                            if (current.getNodeName().equalsIgnoreCase("title")) {
                                title = current.getTextContent();
                            } else if (current.getNodeName().equalsIgnoreCase("description")) {
                                description = Parser.description(current, mCategoryId);
                            } else if (current.getNodeName().equalsIgnoreCase("pubDate")) {
                                pubDate = Parser.getDate(current.getTextContent(), mPattern);
                            } else if (current.getNodeName().equalsIgnoreCase("link")) {
                                link = current.getTextContent();
                            } else if (current.getNodeName().equalsIgnoreCase("image")) {
                                image = Parser.getImage(current, mCategoryId);
                            } else if (current.getNodeName().equalsIgnoreCase("media:thumbnail")) {
                                image = Parser.getImage(current.getAttributes().item(0), mCategoryId);
                            } else if (current.getNodeName().equalsIgnoreCase("media:content")) {
                                image = Parser.getImage(current.getAttributes().item(0), mCategoryId);
                            } else if (current.getNodeName().equalsIgnoreCase("img640x360")) {
                                image = Parser.getImage(current, mCategoryId);
                            }
                        }

                        mTableNews.addRow(
                            link,
                            title,
                            description,
                            pubDate,
                            image,
                            mCategoryId,
                            mSubCategoryId
                        );
                    }
                }
            }
        }
    }
}
